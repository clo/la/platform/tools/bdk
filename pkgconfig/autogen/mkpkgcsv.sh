#!/bin/bash
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# This is a simple bootstrapping script to generate a first pass
# at the pkgconfig csv file. There is some information that this
# script does not capture since not every library properly uses
# export/import includes in their build. As such, the official
# pkgconfig csv file is manually maintained and updated.

ALL_LIBS="\
libbase \
libbinder \
libbrillo \
libbrillo-binder \
libbrillo-dbus \
libbrillo-http \
libbrillo-minijail \
libbrillo-policy \
libbrillo-stream \
libcap \
libcrypto \
libcutils \
libdaemon \
libdbus \
libhardware \
libiprouteutil \
libminijail \
libpcre \
libpowermanager \
librootdev \
libselinux \
libssl \
libweave \
libweaved \
libz"

echo "Rows format: <lib, deps, includes>. "\
"A/B/.. designates wanting only B when A has several subdirs." > "packages.csv"

for lib in ${ALL_LIBS}; do
  intermediates_dir=${ANDROID_PRODUCT_OUT}/obj/SHARED_LIBRARIES/${lib}_intermediates
  export_file=${intermediates_dir}/export_includes
  include_paths=
  if [ -f "${export_file}" ]; then
      include_paths="$(cat ${export_file} | tr -s ' ' '\n' | grep -v -- '^\(-I\)\?$' | tr -s '\n' ' ')"
  fi
  import_file=${intermediates_dir}/import_includes
  deps=
  if [ -f "${import_file}" ]; then
      deps="$(cat ${import_file} | tr -s ' ' '\n' | grep -v -- '^\(-I\)\?$' |
 sed 's:\(external/\([a-z]*\)\)\?/\?.*:\2:' | sed 's:libcxx.*::' | sort | uniq | tr -s '\n' ' ')"
  fi

  normalized_lib=${lib#lib}
  echo "${lib}, ${deps}, ${include_paths}" >> "packages.csv"
done
