# The Brillo Developer Kit (BDK)

This is the bdk which is used to build Brillo.

# Directory Listing

* [cli/](/cli/): The main body of code that makes up the `bdk` tool.
* [schema/](/schema/): Specification for the user's project.xml files.
* [CHANGES](/CHANGES.md): A log highlighting major features across releases.
* [VERSION](/VERSION): This file should be the source of truth of the version
  number for a checkout.  It will be updated by Builders and used by tools
  needing version information.
