#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Unittests for classes in sysroot_util.py"""


import unittest

from environment import sysroot_stub
from environment import sysroot_util
from project import platform_stub
from test import stubs


class SysrootUtilTest(unittest.TestCase):

    OS_VERSION = '65.43'

    def setUp(self):
        self.stub_os = stubs.StubOs()
        self.stub_open = stubs.StubOpen(self.stub_os)
        self.stub_shutil = stubs.StubShutil(self.stub_os)

        sysroot_util.os = self.stub_os
        sysroot_util.open = self.stub_open.open
        sysroot_util.shutil = self.stub_shutil

        self.sysroot = sysroot_stub.StubSysroot('sysroot/path')
        self.platform = platform_stub.StubPlatform(os_version=self.OS_VERSION)
        self.sysrt_util = sysroot_util.SysrootUtil(
            self.sysroot, self.platform)

    def test_add_pc_file(self):
        self.sysroot.should_write = [
            self.stub_os.path.join(self.sysrt_util.PC_DIR, 'libtest.pc')]
        # pylint: disable=protected-access
        self.sysrt_util._AddPkgConfigFile('libtest', 'liba libb', '1.0')
        self.assertTrue('liba libb' in self.sysroot.last_written)
        self.assertTrue('1.0' in self.sysroot.last_written)
        self.assertTrue('-ltest' in self.sysroot.last_written)
        self.assertTrue('libtest' in self.sysroot.last_written)

    def test_add_shared_lib(self):
        self.sysroot.should_write = [
            self.stub_os.path.join(self.sysrt_util.PC_DIR,
                                   'libtest-suffix.pc')]
        lib_dir = self.sysrt_util.DEFAULT_LIB_DIR
        include_dir = self.stub_os.path.join(
            self.sysrt_util.DEFAULT_INCLUDE_DIR,
            self.sysrt_util.ADDITIONAL_INCLUDE_EXTENSION, 'libtest-suffix')
        self.sysroot.should_makedirs = [lib_dir, include_dir]
        self.sysroot.should_add_file = [
            (self.stub_os.path.join('libsrc', 'libtest.so'),
             self.stub_os.path.join(lib_dir, 'libtest-suffix.so'))]
        self.sysroot.should_add_dir = [
            (self.platform.os.path('headers/1'), include_dir, True),
            (self.platform.os.path('headers/deeper/2/../2'),
             self.stub_os.path.join(include_dir, '2'), True)]
        self.sysroot.should_pass_filter = ['file.h', 'h.h', '.h.h',
                                           'longer/path/to/thing.h']
        self.sysroot.should_fail_filter = ['h.cpp', 'h', 'file.h.tar']
        self.sysrt_util.AddSharedLib('libsrc',
                                     'libtest',
                                     'liba libb',
                                     ['headers/1', 'headers/deeper/2/..'],
                                     'suffix')
        # Make sure everything got done.
        self.assertEqual(self.sysroot.should_add_file, [])
        self.assertEqual(self.sysroot.should_add_dir, [])
        self.assertEqual(self.sysroot.should_write, [])

    def test_add_libs_from_csv(self):
        self.sysroot.should_write = [
            self.stub_os.path.join(self.sysrt_util.PC_DIR, 'libtest-suffix.pc'),
            self.stub_os.path.join(self.sysrt_util.PC_DIR,
                                   'libtest2-suffix.pc')]
        lib_dir = self.sysrt_util.DEFAULT_LIB_DIR
        include_dir = self.stub_os.path.join(
            self.sysrt_util.DEFAULT_INCLUDE_DIR,
            self.sysrt_util.ADDITIONAL_INCLUDE_EXTENSION, 'libtest-suffix')
        include_dir2 = self.stub_os.path.join(
            self.sysrt_util.DEFAULT_INCLUDE_DIR,
            self.sysrt_util.ADDITIONAL_INCLUDE_EXTENSION, 'libtest2-suffix')
        self.sysroot.should_makedirs = [lib_dir, include_dir, include_dir2]
        self.sysroot.should_add_file = [
            (self.stub_os.path.join('libsrc', 'libtest.so'),
             self.stub_os.path.join(lib_dir, 'libtest-suffix.so')),
            (self.stub_os.path.join('libsrc', 'libtest2.so'),
             self.stub_os.path.join(lib_dir, 'libtest2-suffix.so'))]
        self.sysroot.should_add_dir = [
            (self.platform.os.path('headers/1'), include_dir, True),
            (self.platform.os.path('headers/2'), include_dir, True)]
        self.sysroot.should_pass_filter = ['file.h', 'h.h', '.h.h',
                                           'longer/path/to/thing.h']
        self.sysroot.should_fail_filter = ['h.cpp', 'h', 'file.h.tar']
        f = stubs.StubFile('header line\n'
                           'libtest, liba libb, headers/1 headers/2\n'
                           'libtest2, , ')
        self.stub_open.files['csv_path'] = f
        self.stub_os.path.should_exist = ['csv_path']

        self.sysrt_util.AddSharedLibsFromCSV('libsrc', 'csv_path', 'suffix')
        # Make sure everything got done.
        self.assertEqual(self.sysroot.should_add_file, [])
        self.assertEqual(self.sysroot.should_add_dir, [])
        self.assertEqual(self.sysroot.should_write, [])


    def test_add_libs_bad_csv(self):
        f = stubs.StubFile('header line\n'
                           'has, enough, items\n'
                           'not, enough items')
        self.stub_open.files['csv_path'] = f
        self.stub_os.path.should_exist = ['csv_path']
        # Error should indicate which line was problematic.
        self.assertRaisesRegexp(ValueError, 'not, enough items',
                                self.sysrt_util.AddSharedLibsFromCSV,
                                'libsrc', 'csv_path', 'suffix')

    def test_setup_base(self):
        self.sysroot.should_makedirs = [self.sysrt_util.DEFAULT_INCLUDE_DIR,
                                        self.sysrt_util.DEFAULT_LIB_DIR]
        def copyformat(src):
            # pylint: disable=protected-access
            return self.sysrt_util._FormatForCopy(
                src, self.sysrt_util.DEFAULT_INCLUDE_DIR)

        # pylint: disable=protected-access
        (libcxx_src, libcxx_dest) = self.sysrt_util._FormatForCopy(
            self.sysrt_util.LIBCXX_HEADERS, self.sysrt_util.LIBCXX_INCLUDE_DIR)

        self.sysroot.should_add_dir = [(libcxx_src, libcxx_dest, True)]
        for (src, recurse, _) in (self.sysrt_util.DEFAULT_INCLUDES +
                                  self.sysrt_util.DEFAULT_ARCH_INCLUDES['x86']):
            (copysrc, copydest) = copyformat(src)
            self.sysroot.should_add_dir.append((copysrc, copydest, recurse))
        self.sysroot.should_add_file = [('lib_src/' + default,
                                         self.sysrt_util.DEFAULT_LIB_DIR)
                                        for default
                                        in self.sysrt_util.DEFAULT_SHARED_LIBS]
        self.sysroot.should_add_file += [
            ('static_src/' + self.sysrt_util.STATIC_PATH_FORMAT.format(default),
             self.sysrt_util.DEFAULT_LIB_DIR)
            for default in self.sysrt_util.DEFAULT_STATIC_LIBS]
        self.sysroot.should_add_dir.append(('base', '', True))
        self.stub_shutil.should_move = [
            (self.sysroot.Path(self.sysrt_util.DEFAULT_LIB_DIR, src),
             self.sysroot.Path(self.sysrt_util.DEFAULT_LIB_DIR, dest))
            for (src, dest) in
            self.sysrt_util.DEFAULT_LIB_RENAMES.iteritems()]

        self.sysrt_util.SetupBaseSysroot('x86', 'lib_src', 'static_src', 'base')
        # Make sure everything got done.
        self.assertEqual(self.sysroot.should_add_file, [])
        self.assertEqual(self.sysroot.should_add_dir, [])
        self.assertEqual(self.sysroot.should_write, [])
        self.assertEqual(self.stub_shutil.should_move, [])
