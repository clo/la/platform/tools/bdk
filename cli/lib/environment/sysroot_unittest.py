#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Unittests for classes in sysroot.py"""


import unittest

from environment import sysroot
from test import stubs


class SysrootTest(unittest.TestCase):
    def setUp(self):
        self.stub_os = stubs.StubOs()
        self.stub_glob = stubs.StubGlob(self.stub_os)
        self.stub_shutil = stubs.StubShutil(self.stub_os)
        self.stub_open = stubs.StubOpen(self.stub_os)

        sysroot.os = self.stub_os
        sysroot.glob = self.stub_glob
        sysroot.shutil = self.stub_shutil
        sysroot.open = self.stub_open.open

    def gen_sysroot(self, copy_newer_only=False):
        """Helper - Generates a sysroot and returns it and its path."""
        path = 'path/subdir'
        self.stub_os.should_makedirs = ['path/subdir']
        return (sysroot.Sysroot(path, copy_newer_only=copy_newer_only), path)

    def test_init_destroy(self):
        self.stub_os.path.should_be_dir = []
        # Check that necessary dirs are created for a sysroot on init.
        (sysrt, _) = self.gen_sysroot()
        self.assertTrue(self.stub_os.path.isdir('path'))
        self.assertTrue(self.stub_os.path.isdir('path/subdir'))
        sysrt.Destroy()
        # 'path' will actually remain, but this is WAI because it may
        # have had non-sysroot files added between creation and destruction.
        self.assertTrue(self.stub_os.path.isdir('path'))
        self.assertFalse(self.stub_os.path.isdir('path/subdir'))

    def test_path(self):
        self.stub_os.path.should_be_dir = []
        (sysrt, path) = self.gen_sysroot()
        self.assertEqual(path, sysrt.Path())
        self.assertEqual(self.stub_os.path.join(path, 'boogie', 'woogie',
                                                'woo'),
                         sysrt.Path('boogie', 'woogie', 'woo'))

    def test_makedirs_hasdir(self):
        self.stub_os.path.should_be_dir = []
        (sysrt, path) = self.gen_sysroot()

        self.stub_os.should_makedirs = [sysrt.Path('deeper/deeperer/deepest')]
        # Makedirs
        sysrt.Makedirs('deeper/deeperer/deepest')
        self.assertTrue(self.stub_os.path.isdir(
            self.stub_os.path.join(path, 'deeper')))
        self.assertTrue(
            self.stub_os.path.isdir(self.stub_os.path.join(path,
                                                           'deeper/deeperer')))
        self.assertTrue(self.stub_os.path.isdir(
            self.stub_os.path.join(path, 'deeper/deeperer/deepest')))
        # Trying to make existing dirs doesn't cause a problem
        sysrt.Makedirs('deeper/deeperer')

        # HasDir
        self.assertTrue(sysrt.HasDir('deeper'))
        self.assertTrue(sysrt.HasDir('deeper/deeperer'))
        self.assertTrue(sysrt.HasDir('deeper/deeperer/deepest'))

    def test_add_file(self):
        self.stub_os.path.should_be_dir = []
        (sysrt, _) = self.gen_sysroot()
        self.stub_shutil.should_copy = [('a/b/c', sysrt.Path('d'))]
        self.stub_os.path.should_exist += ['a/b/c']
        sysrt.AddFile('a/b/c', 'd')
        self.assertEqual(self.stub_shutil.should_copy, [])
        # If we fail, should raise IOError
        self.assertRaisesRegexp(IOError, 'does/not/exist', sysrt.AddFile,
                                'does/not/exist', 'e')

    def test_add_file_cond_new(self):
        self.stub_os.path.should_be_dir = []
        (sysrt, _) = self.gen_sysroot(copy_newer_only=True)
        self.stub_shutil.should_copy = [('a/b/c', sysrt.Path('d'))]
        self.stub_os.path.should_exist += ['a/b/c']
        # There is no destination, so this is just a normal copy.
        sysrt.AddFile('a/b/c', 'd')
        self.assertEqual(self.stub_shutil.should_copy, [])
        # If we fail, should raise IOError
        self.assertRaisesRegexp(IOError, 'does/not/exist', sysrt.AddFile,
                                'does/not/exist', 'e')

    def test_add_file_cond_exists_but_newer(self):
        self.stub_os.path.should_be_dir = []
        (sysrt, _) = self.gen_sysroot(copy_newer_only=True)
        self.stub_os.path.should_exist = [sysrt.Path('d'), 'a/b/c']
        # No copy should occur.
        self.stub_os.should_stat = {sysrt.Path('d'): {'st_mtime': 100},
                                    'a/b/c': {'st_mtime': 100}}
        sysrt.AddFile('a/b/c', 'd')
        self.assertEqual(self.stub_shutil.should_copy, [])
        # If we fail, should raise IOError
        self.assertRaisesRegexp(IOError, 'does/not/exist', sysrt.AddFile,
                                'does/not/exist', 'e')

    def test_add_file_cond_exists_and_older(self):
        self.stub_os.path.should_be_dir = []
        (sysrt, _) = self.gen_sysroot(copy_newer_only=True)
        self.stub_shutil.should_copy = [('a/b/c', sysrt.Path('d'))]
        self.stub_os.path.should_exist = [sysrt.Path('d'), 'a/b/c']
        # 'd' will be copied over.
        self.stub_os.should_stat = {sysrt.Path('d'): {'st_mtime': 99},
                                    'a/b/c': {'st_mtime': 100}}
        sysrt.AddFile('a/b/c', 'd')
        self.assertEqual(self.stub_shutil.should_copy, [])
        # If we fail, should raise IOError
        self.assertRaisesRegexp(IOError, 'does/not/exist', sysrt.AddFile,
                                'does/not/exist', 'e')

    def test_add_symlink(self):
        self.stub_os.path.should_be_dir = []
        (sysrt, _) = self.gen_sysroot()
        self.stub_shutil.should_copy = [('a/b/c', sysrt.Path('d'))]
        self.stub_os.path.should_exist += ['a/b/c']
        sysrt.AddFile('a/b/c', 'd')
        self.assertEqual(self.stub_shutil.should_copy, [])
        self.stub_os.path.should_be_dir += ['root']
        self.stub_os.path.should_exist += ['root', 'root/link']
        self.stub_os.path.should_link['root/link'] = '../a_link_dest'

        # Should copy not_a_link, and create a link for link.
        self.stub_os.should_create_link = [
            ('../a_link_dest', sysrt.Path('newlink')),
            ('../../relative_link_dest', sysrt.Path('subdir/link')),
        ]
        sysrt.AddSymlink('root/link', 'newlink')
        # Make sure the one file was copied, and the link was created.
        self.assertEqual('../a_link_dest',
                         self.stub_os.readlink(sysrt.Path('newlink')))
        # If we fail, should raise IOError
        self.assertRaisesRegexp(IOError, 'does/not/exist', sysrt.AddSymlink,
                                'does/not/exist', 'e')

    def test_add_glob(self):
        self.stub_os.path.should_be_dir = []
        self.stub_os.path.should_exist = []
        (sysrt, _) = self.gen_sysroot()
        directories = ['root', 'root/sub1', 'root/sub2', 'root/sub1/supersub']
        files = ['root/sub1/copy1', 'root/sub2/copy2',
                 'root/sub1/supersub/copy3', 'root/copy0', 'root/copy1']
        for directory in directories:
            self.stub_os.path.should_be_dir.append(directory)
            self.stub_os.path.should_exist.append(directory)
        for f in files:
            self.stub_os.path.should_exist.append(f)
        # Should only copy from top level of root matching copy*.
        self.stub_shutil.should_copy.append(('root/copy0', sysrt.Path('copy0')))
        self.stub_shutil.should_copy.append(('root/copy1', sysrt.Path('copy1')))
        sysrt.AddGlob('root/copy*', recurse=False)
        # Make sure the two files were copied.
        self.assertEqual(self.stub_shutil.should_copy, [])

    def test_add_glob_no_match(self):
        self.stub_os.path.should_be_dir = []
        self.stub_os.path.should_exist = []
        (sysrt, _) = self.gen_sysroot()
        directories = ['root', 'root/sub1', 'root/sub2', 'root/sub1/supersub']
        files = ['root/sub1/copy1', 'root/sub2/copy2',
                 'root/sub1/supersub/copy3']
        for directory in directories:
            self.stub_os.path.should_be_dir.append(directory)
            self.stub_os.path.should_exist.append(directory)
        for f in files:
            self.stub_os.path.should_exist.append(f)
        with self.assertRaises(IOError):
            sysrt.AddGlob('root/copy*', recurse=False)

    def test_add_glob_recurse(self):
        self.stub_os.path.should_exist = []
        self.stub_os.path.should_be_dir = []
        (sysrt, _) = self.gen_sysroot()
        directories = ['root', 'root/sub1', 'root/sub2', 'root/sub1/supersub']
        files = ['root/sub1/copy1', 'root/sub2/copy2',
                 'root/sub1/supersub/copy3', 'root/copy0']
        for directory in directories:
            self.stub_os.path.should_be_dir.append(directory)
            self.stub_os.path.should_exist.append(directory)
            self.stub_os.should_makedirs.append(
                sysrt.Path(directory.replace('root', 'dest')))
        for f in files:
            self.stub_os.path.should_exist.append(f)
            self.stub_shutil.should_copy.append(
                (f, sysrt.Path(f.replace('root', 'dest'))))
        # Add some files to the mock filesystem that shouldn't be copied.
        self.stub_os.path.should_exist.append('root/donotcopy')
        self.stub_os.path.should_exist.append('other')
        self.stub_os.path.should_be_dir.append('other')
        self.stub_os.path.should_exist.append('other/copy0')
        self.stub_os.path.should_exist.append('other/unfilteredcopy')
        # A filter that should only collect the copy files.
        def copy_filter(name):
            return name.startswith('copy')

        sysrt.AddGlob('root/*', 'dest', recurse=True, filter_func=copy_filter)
        # Make sure everything was copied.
        self.assertEqual(self.stub_shutil.should_copy, [])
        # Note: shutil copy2 stub is not implemented such that it checks for
        # dirs existing.  This will test that dirs were created, but not
        # necessarily that they were created before they were copied to.
        for directory in directories:
            self.assertTrue(
                sysrt.HasDir(self.stub_os.path.join(directory.replace('root',
                                                                      'dest'))))

        # Test with no filter.
        # Also should be no issue copying into a now-populated destination.
        self.stub_shutil.should_copy.append(('other/copy0',
                                             sysrt.Path('dest', 'copy0')))
        sysrt.AddGlob('other/c*', 'dest')
        # Make sure everything was copied.
        self.assertEqual(self.stub_shutil.should_copy, [])

    def test_add_glob_symlinks(self):
        (sysrt, _) = self.gen_sysroot()
        self.stub_os.path.should_be_dir += ['root', 'root/subdir']
        self.stub_os.should_makedirs += [sysrt.Path('subdir')]
        self.stub_os.path.should_exist += [
            'root', 'root/subdir', 'root/subdir/not_a_link', 'root/link',
            'root/subdir/link']
        self.stub_os.path.should_link['root/link'] = '../relative_link_dest'
        self.stub_os.path.should_link['root/subdir/link'] = (
            '../../relative_link_dest')

        # Should copy not_a_link, and create a link for link.
        self.stub_os.should_create_link = [
            ('../relative_link_dest', sysrt.Path('link')),
            ('../../relative_link_dest', sysrt.Path('subdir/link')),
        ]
        self.stub_shutil.should_copy = [
            ('root/subdir/not_a_link', sysrt.Path('subdir/not_a_link'))]
        sysrt.AddGlob('root/*', recurse=True, symlinks=True)
        # Make sure the one file was copied, and the link was created.
        self.assertEqual(self.stub_shutil.should_copy, [])
        self.assertEqual('../relative_link_dest',
                         self.stub_os.readlink(sysrt.Path('link')))
        self.assertEqual('../../relative_link_dest',
                         self.stub_os.readlink(sysrt.Path('subdir/link')))

    def test_add_glob_ignore_symlinks(self):
        (sysrt, _) = self.gen_sysroot()
        self.stub_os.path.should_be_dir += ['root', 'root/subdir']
        self.stub_os.should_makedirs += [sysrt.Path('subdir')]
        self.stub_os.path.should_exist += [
            'root', 'root/subdir', 'root/not_a_link', 'root/subdir/link']
        self.stub_os.path.should_link['root/subdir/link'] = (
            '../relative_link_dest')

        # Should copy both not_a_link and link.
        self.stub_shutil.should_copy = [
            ('root/not_a_link', sysrt.Path('not_a_link')),
            ('root/subdir/link', sysrt.Path('subdir/link'))]
        sysrt.AddGlob('root/*', recurse=True, symlinks=False)
        # Make sure the two files were copied.
        self.assertEqual(self.stub_shutil.should_copy, [])

    def test_add_dir_recurse_and_unfiltered(self):
        self.stub_os.path.should_exist = []
        self.stub_os.path.should_be_dir = []
        (sysrt, _) = self.gen_sysroot()
        directories = ['root', 'root/sub1', 'root/sub2', 'root/sub1/supersub']
        files = ['root/sub1/copy1', 'root/sub2/copy2',
                 'root/sub1/supersub/copy3', 'root/copy0']
        for directory in directories:
            self.stub_os.path.should_be_dir.append(directory)
            self.stub_os.path.should_exist.append(directory)
            self.stub_os.should_makedirs.append(
                sysrt.Path(directory.replace('root', 'dest')))
        for f in files:
            self.stub_os.path.should_exist.append(f)
            self.stub_shutil.should_copy.append(
                (f, sysrt.Path(f.replace('root', 'dest'))))
        # Add some files to the mock filesystem that shouldn't be copied.
        self.stub_os.path.should_exist.append('root/donotcopy')
        self.stub_os.path.should_exist.append('other')
        self.stub_os.path.should_be_dir.append('other')
        self.stub_os.path.should_exist.append('other/copy0')
        self.stub_os.path.should_exist.append('other/unfilteredcopy')
        # A filter that should only collect the copy files.
        def copy_filter(name):
            return name.startswith('copy')

        sysrt.AddDir('root', 'dest', recurse=True, filter_func=copy_filter)
        # Make sure everything was copied.
        self.assertEqual(self.stub_shutil.should_copy, [])
        # Note: shutil copy2 stub is not implemented such that it checks for
        # dirs existing. This will test that dirs were created, but not
        # necessarily that they were created before they were copied to.
        for directory in directories:
            self.assertTrue(
                sysrt.HasDir(
                    self.stub_os.path.join(directory.replace('root', 'dest'))))

        # Test with no filter.
        # Also should be no issue copying into a now-populated destination.
        self.stub_shutil.should_copy.append(
            ('other/copy0', sysrt.Path('dest', 'copy0')))
        self.stub_shutil.should_copy.append(('other/unfilteredcopy',
                                             sysrt.Path('dest',
                                                        'unfilteredcopy')))
        sysrt.AddDir('other', 'dest')
        # Make sure everything was copied.
        self.assertEqual(self.stub_shutil.should_copy, [])

    def test_add_dir_top_level_no_recurse(self):
        (sysrt, _) = self.gen_sysroot()
        directories = ['root', 'root/sub1', 'root/sub2', 'root/sub1/supersub']
        files = ['root/sub1/copy1', 'root/sub2/copy2',
                 'root/sub1/supersub/copy3', 'root/copy0']
        for directory in directories:
            self.stub_os.path.should_be_dir.append(directory)
            self.stub_os.path.should_exist.append(directory)
        for f in files:
            self.stub_os.path.should_exist.append(f)
        # Should only copy from top level of root,
        # and with no dest provided should copy to top level of the sysroot.
        self.stub_shutil.should_copy.append(('root/copy0', sysrt.Path('copy0')))
        sysrt.AddDir('root', recurse=False)
        # Make sure the one file was copied.
        self.assertEqual(self.stub_shutil.should_copy, [])

    def test_add_dir_symlinks(self):
        (sysrt, _) = self.gen_sysroot()
        self.stub_os.path.should_be_dir += ['root', 'root/subdir']
        self.stub_os.should_makedirs += [sysrt.Path('subdir')]
        self.stub_os.path.should_exist += [
            'root', 'root/subdir', 'root/not_a_link', 'root/subdir/link']
        self.stub_os.path.should_link['root/subdir/link'] = (
            '../relative_link_dest')

        # Should copy not_a_link, and create a link for link.
        self.stub_os.should_create_link = [('../relative_link_dest',
                                            sysrt.Path('subdir/link'))]
        self.stub_shutil.should_copy = [
            ('root/not_a_link', sysrt.Path('not_a_link'))]
        sysrt.AddDir('root', symlinks=True)
        # Make sure the one file was copied, and the link was created.
        self.assertEqual(self.stub_shutil.should_copy, [])
        self.assertEqual('../relative_link_dest',
                         self.stub_os.readlink(sysrt.Path('subdir/link')))

    def test_add_dir_ignore_symlinks(self):
        (sysrt, _) = self.gen_sysroot()
        self.stub_os.path.should_be_dir += ['root', 'root/subdir']
        self.stub_os.should_makedirs += [sysrt.Path('subdir')]
        self.stub_os.path.should_exist += [
            'root', 'root/subdir', 'root/not_a_link', 'root/subdir/link']
        self.stub_os.path.should_link['root/subdir/link'] = (
            '../relative_link_dest')

        # Should copy both not_a_link and link.
        self.stub_shutil.should_copy += [
            ('root/not_a_link', sysrt.Path('not_a_link')),
            ('root/subdir/link', sysrt.Path('subdir/link'))]
        sysrt.AddDir('root', symlinks=False)
        # Make sure the two files were copied.
        self.assertEqual(self.stub_shutil.should_copy, [])

    def test_write_file(self):
        self.stub_os.path.should_be_dir = []
        (sysrt, _) = self.gen_sysroot()
        self.stub_os.should_makedirs = [sysrt.Path('dir')]
        sysrt.WriteFile('dir/file', 'contents')
        self.assertTrue(
            'contents' in self.stub_open.files[sysrt.Path('dir/file')].contents)
