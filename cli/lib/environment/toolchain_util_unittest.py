#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Unittests for methods in toolchain_util.py"""


import stat
import unittest

from core import util_stub
from environment import toolchain_util
from project import platform_stub
from test import stubs


class ToolchainUtilTest(unittest.TestCase):

    OS_VERSION = '43.21'
    TARGET_ARCH = 'x86'
    HOST_ARCH = 'host_arch'

    def setUp(self):
        self.stub_os = stubs.StubOs()
        self.stub_open = stubs.StubOpen(self.stub_os)
        self.stub_glob = stubs.StubGlob(self.stub_os)
        self.stub_util = util_stub.StubUtil()

        toolchain_util.os = self.stub_os
        toolchain_util.open = self.stub_open.open
        toolchain_util.glob = self.stub_glob
        toolchain_util.util = self.stub_util

        self.platform = platform_stub.StubPlatform(
            os_version=self.OS_VERSION,
            device_arch=self.TARGET_ARCH)


    def test_generate_toolchain(self):
        stub_tools = ['no_flags', 'g++']
        tool_paths = {}
        tool_dests = {}
        for tool in stub_tools:
            tool_paths[tool] = self.platform.os.path(
                toolchain_util.EXISTING_TOOLS_FORMAT.format(
                    host_arch=self.HOST_ARCH),
                toolchain_util.ARCH_TOOL_PREFIX[self.TARGET_ARCH] + tool)
            self.stub_os.path.should_exist.append(tool_paths[tool])

            tool_dests[tool] = self.stub_os.path.join('dest', tool)
            self.stub_os.should_chmod.append((tool_dests[tool], stat.S_IEXEC))

        self.stub_os.should_makedirs = ['dest']
        toolchain_util.GenerateToolchain(self.platform, self.HOST_ARCH, 'dest')
        # Should have created dest dir.
        self.assertTrue(self.stub_os.path.isdir('dest'))
        for tool in stub_tools:
            # Should have generated the tool file.
            self.assertTrue(self.stub_os.path.isfile(tool_dests[tool]))
            # Should have correctly generated the tool file.
            tool_file = self.stub_open.files[tool_dests[tool]]
            self.assertEqual(len(tool_file.contents), 3)
            # Should invoke sh.
            self.assertEqual(tool_file.contents[0], '#!/bin/sh')
            # Should be a nice file with a trailing newline.
            self.assertEqual(tool_file.contents[2], '')
            # Should point to the original tool.
            self.assertTrue(tool_paths[tool] in tool_file.contents[1])

            if tool == 'g++':
                # g++ is supposed to get some flags. Make sure those appeared.
                # Note: this is not comprehensive testing of all the flags.
                # Non arch-specific:
                self.assertTrue('-Wformat' in tool_file.contents[1])
                # Arch-specific:
                self.assertTrue('-m32' in tool_file.contents[1])

        # All chmods should have happened.
        self.assertEqual(self.stub_os.should_chmod, [])
