#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""A class storing user configuration."""


import uuid

from core import config
from core import util


BSP_DIR = 'bsp_dir'
UID = 'uid'
OS_ROOT = 'os_root'
PLATFORM_CACHE = 'platform_cache'
METRICS_OPT_IN = 'metrics_opt_in'
# Don't expose uid.
EXPOSED = [BSP_DIR, OS_ROOT, PLATFORM_CACHE, METRICS_OPT_IN]
PATHS = [BSP_DIR, OS_ROOT, PLATFORM_CACHE]


class UserConfig(config.Store):

    REQUIRED_PROPS = {METRICS_OPT_IN:['0', '1'], UID:[]}
    OPTIONAL_PROPS = {BSP_DIR:[], OS_ROOT:[], PLATFORM_CACHE:[]}
    PREFIX = 'user_'

    def __init__(self, file_path='', table='user'):
        file_path = file_path or util.GetUserDataPath('config.db')
        super(UserConfig, self).__init__(file_path, table)

        self._defaults = {
            BSP_DIR: util.GetBDKPath('BSPs'),
            OS_ROOT: util.DEPRECATED_GetDefaultOSPath(),
            PLATFORM_CACHE: util.GetBDKPath('platform_cache')
        }


    def initialize(self, opt_in=None):
        """Sets up the user store.

        Prompts the user for information when necessary.

        Args:
            opt_in: (optional) If provided, skip prompting and set
                metrics_opt_in to the provided value. Fixes True to '1'
                and False to '0'.
        """
        # Fix booleans to strings.
        if opt_in == False:
            opt_in = '0'
        elif opt_in == True:
            opt_in = '1'

        # Check for opt in
        print ('To help improve the quality of this product, we collect\n'
               'anonymized data on how the BDK is used. You may choose to opt\n'
               'out of this collection now (by choosing "N" at the below\n'
               'prompt), or at any point in the future by running the\n'
               'following command:\n'
               '    bdk config set metrics_opt_in 0')
        while opt_in is None:
            choice = util.GetUserInput('Do you want to help improve the '
                                       'Project Brillo BDK?\n'
                                       '(Y/n) ').strip().upper()
            if not choice or choice == 'Y':
                opt_in = '1'
            elif choice == 'N':
                opt_in = '0'

        # pylint: disable=attribute-defined-outside-init
        self.metrics_opt_in = opt_in

        self.uid = str(uuid.uuid4())

    def _save(self, key, val):
        if not val and key in self._defaults:
            val = self._defaults[key]
        return super(UserConfig, self)._save(key, val)

    def _load(self, key):
        result = super(UserConfig, self)._load(key)
        if not result and key in self._defaults:
            result = self._defaults[key]
        return result

# A default global UserConfig.
USER_CONFIG = UserConfig()
