#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Unittests for timer.py"""


import unittest

from core import timer
from test import stubs


class TimerTest(unittest.TestCase):

    def setUp(self):
        self.stub_time = stubs.StubTime()

        timer.time = self.stub_time

    def RunningTimerWithTime(self, time):
        """Returns a timer running with a given amount of time already on it.

        The timer will have been started at stub_time = 0.
        """
        self.stub_time.current_time = 0
        t = timer.Timer('event')
        t.Start()
        self.stub_time.current_time = time
        # Confirm that this is indeed a running timer with the desired time.
        self.assertTrue(t.IsRunning())
        self.assertAlmostEqual(t.Read(), time)
        return t

    def StoppedTimerWithTime(self, time):
        """Returns a timer stopped with a given amount of time already on it."""
        t = self.RunningTimerWithTime(time)
        t.Stop()
        # Confirm that this is indeed a stopped timer with the desired time.
        self.assertFalse(t.IsRunning())
        self.assertAlmostEqual(t.Read(), time)
        return t

    def test_init(self):
        # Time at init shouldn't matter.
        self.stub_time.current_time = 123.45
        t = timer.Timer('event')
        self.assertFalse(t.IsRunning())
        self.assertAlmostEqual(t.Read(), 0)

    def test_stopped_read(self):
        t = self.StoppedTimerWithTime(12.3)
        self.assertAlmostEqual(t.Read(), 12.3)
        # Reading shouldn't change running state.
        self.assertFalse(t.IsRunning())

        # Even if time is progressed, it should still read the same value.
        self.stub_time.current_time = 45.6
        self.assertAlmostEqual(t.Read(), 12.3)

    def test_stopped_start(self):
        t = self.StoppedTimerWithTime(12.3)
        t.Start()
        self.assertTrue(t.IsRunning())
        # Should not reset time.
        self.assertAlmostEqual(t.Read(), 12.3)

    def test_stopped_reset(self):
        t = self.StoppedTimerWithTime(12.3)
        t.Reset()
        # Reset should stop and set to 0.
        self.assertFalse(t.IsRunning())
        self.assertAlmostEqual(t.Read(), 0)

    def test_stopped_stop(self):
        t = self.StoppedTimerWithTime(12.3)
        t.Stop()
        # Stop should have no further effect.
        self.assertFalse(t.IsRunning())
        self.assertAlmostEqual(t.Read(), 12.3)

    def test_running_read(self):
        t = self.RunningTimerWithTime(12.3)
        self.assertAlmostEqual(t.Read(), 12.3)
        # Read should not change running state.
        self.assertTrue(t.IsRunning())

        # If time is progressed, it should read the appropriate value.
        self.stub_time.current_time = 45.6
        self.assertAlmostEqual(t.Read(), 45.6)

    def test_running_start(self):
        t = self.RunningTimerWithTime(12.3)
        t.Start()
        # Should have no effect.
        self.assertTrue(t.IsRunning())
        self.assertAlmostEqual(t.Read(), 12.3)

    def test_running_reset(self):
        t = self.RunningTimerWithTime(12.3)
        t.Reset()
        # Reset should stop and set to 0.
        self.assertFalse(t.IsRunning())
        self.assertAlmostEqual(t.Read(), 0)

    def test_running_stop(self):
        t = self.RunningTimerWithTime(12.3)
        t.Stop()
        # Stop should stop but maintain time.
        self.assertFalse(t.IsRunning())
        self.assertAlmostEqual(t.Read(), 12.3)

    def test_combined_read(self):
        t = self.StoppedTimerWithTime(12.3)
        self.stub_time.current_time = 20
        t.Start()
        self.stub_time.current_time = 45.6
        expected_read_time = 12.3 + (45.6 - 20)
        # When a timer has been previously stopped, current running time should
        # be additive.
        self.assertAlmostEqual(t.Read(), expected_read_time)
        # Stopping again shouldn't overwrite this number.
        t.Stop()
        self.stub_time.current_time = 56.7
        self.assertAlmostEqual(t.Read(), expected_read_time)
