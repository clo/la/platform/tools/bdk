#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Tests for core/provision.py."""


import os
import unittest

from core import provision
from core import tool
from core import util
from core import util_stub
import error
from project import platform_stub
from test import stubs


class ProvisionDeviceTest(unittest.TestCase):
    """Tests for provision_device()."""

    _BSP = 'boardname'
    _CUSTOM_SYSTEM_IMAGE_DIR = '/foo'
    _CUSTOM_SYSTEM_IMAGE_PATH = os.path.join(_CUSTOM_SYSTEM_IMAGE_DIR,
                                             'system.img')
    _HOST_ARCH = 'foo_arch'
    _OS_ROOT = '/os/tree'
    _OS_VERSION = '55.55'
    _BUILD_TYPE = 'userdebug'

    # Necessary paths derived from our constants.
    _PLATFORM_BUILD_OUT = os.path.join('/platform', _BUILD_TYPE)
    _PRODUCT_BUILD_OUT = os.path.join(_PLATFORM_BUILD_OUT, 'target', 'product',
                                      _BSP)
    _PROVISION_DEVICE_PATH = os.path.join(_PRODUCT_BUILD_OUT,
                                          'provision-device')
    _DEFAULT_SYSTEM_IMAGE_PATH = os.path.join(_PRODUCT_BUILD_OUT, 'system.img')

    _HOST_BUILD_OUT = os.path.join(_PLATFORM_BUILD_OUT, 'host', _HOST_ARCH)
    _FASTBOOT_PATH = os.path.join(_HOST_BUILD_OUT, 'bin', 'fastboot')


    def setUp(self):
        """Overrides imported modules with stubs."""
        self.stub_os = stubs.StubOs()
        self.stub_shutil = stubs.StubShutil(self.stub_os)
        self.stub_tempfile = stubs.StubTempfile(self.stub_os)
        self.stub_subprocess = stubs.StubSubprocess()
        self.stub_util = util_stub.StubUtil(arch=self._HOST_ARCH)

        provision.os = self.stub_os
        provision.shutil = self.stub_shutil
        provision.tempfile = self.stub_tempfile
        provision.util = self.stub_util

        provision.tool.os = self.stub_os
        provision.tool.subprocess = self.stub_subprocess
        provision.tool.util = self.stub_util

        self.platform = platform_stub.StubPlatform(
            board=self._BSP, os_version=self._OS_VERSION,
            build_cache=self._PLATFORM_BUILD_OUT,
            product_out_cache=self._PRODUCT_BUILD_OUT,
            build_type=self._BUILD_TYPE, os_root=self._OS_ROOT)

        # Give more obvious names to some commonly used variables.
        self.provision_temp_dir = self.stub_tempfile.temp_dir
        self.system_image_link = self.stub_os.path.join(self.provision_temp_dir,
                                                        'system.img')

    def tearDown(self):
        """Performs shared end-of-test logic."""
        # No matter the result, the temp dir should not exist afterwards.
        self.assertFalse(self.stub_os.path.exists(self.provision_temp_dir))
        self.assertFalse(self.stub_os.path.isdir(self.provision_temp_dir))

    def prepare_os_files(self):
        """Creates the necessary files for provision_device()."""
        self.stub_os.path.should_be_dir.append(self._PRODUCT_BUILD_OUT)
        self.stub_os.path.should_exist += [self._FASTBOOT_PATH,
                                           self._PROVISION_DEVICE_PATH,
                                           self._CUSTOM_SYSTEM_IMAGE_PATH]
        self.stub_os.should_create_link.append((
            self._PROVISION_DEVICE_PATH,
            os.path.join(self.provision_temp_dir, 'provision-device')))
        self.stub_os.should_makedirs.append(self.provision_temp_dir)
        self.platform.should_link = True

    def test_call(self):
        """Tests a successful provision-device call."""
        self.prepare_os_files()
        command = self.stub_subprocess.AddCommand()

        provision.provision_device(self.platform)
        # provision-device environment must have:
        #   * PATH contain the fastboot executable.
        #   * ANDROID_BUILD_TOP point to the OS source root.
        #   * ANDROID_PRODUCT_OUT point to our temporary product dir.
        command.AssertCallWas(
            [os.path.join(self.provision_temp_dir, 'provision-device')],
            shell=False, cwd=None,
            stdout=None, stderr=None,
            env={'PATH': self.stub_os.path.dirname(self._FASTBOOT_PATH),
                 'ANDROID_BUILD_TOP': self._OS_ROOT,
                 'ANDROID_HOST_OUT': self._HOST_BUILD_OUT,
                 'ANDROID_PRODUCT_OUT': self.provision_temp_dir})

    def test_caller_env_path(self):
        """Tests that provision-device can access the caller's PATH."""
        self.prepare_os_files()
        self.stub_os.environ['PATH'] = '/foo/bar:/baz'
        command = self.stub_subprocess.AddCommand()

        provision.provision_device(self.platform)
        self.assertEqual(
            self.stub_os.path.dirname(self._FASTBOOT_PATH) + ':/foo/bar:/baz',
            command.GetCallArgs()[1]['env']['PATH'])

    def test_product_build_symlinks(self):
        """Tests that the temp dir symlinks are generated."""
        self.prepare_os_files()
        # Put a few extra files in the product build directory as symlink
        # targets.
        file_names = ('foo.txt', 'system.img', 'cache', 'bar000')
        for name in file_names:
            path = self.stub_os.path.join(self._PRODUCT_BUILD_OUT, name)
            self.stub_os.path.should_exist.append(path)
            self.stub_os.should_create_link.append((
                path, os.path.join(self.provision_temp_dir, name)))

        # Helper function to check that symlinks exist.
        def check_symlinks():
            for name in file_names:
                link_target = self.stub_os.path.join(self._PRODUCT_BUILD_OUT,
                                                     name)
                link_path = self.stub_os.path.join(self.provision_temp_dir,
                                                   name)
                self.assertEqual(link_target, self.stub_os.readlink(link_path))

        # Run the symlink check as a side effect of the subprocess because by
        # the time we get control back here the temp dir will have been cleaned
        # up.
        self.stub_subprocess.AddCommand(side_effect=check_symlinks)

        provision.provision_device(self.platform)

    def test_custom_system_image(self):
        """Tests that a custom system.img is used if provided."""
        self.prepare_os_files()
        self.stub_os.path.should_exist.append(self._DEFAULT_SYSTEM_IMAGE_PATH)
        self.stub_os.should_create_link.append((
            self._CUSTOM_SYSTEM_IMAGE_PATH,
            os.path.join(self.provision_temp_dir, 'system.img')))

        # Check the link points to the custom image, not the default system
        # image.
        def check_custom_image_link():
            self.assertEqual(self._CUSTOM_SYSTEM_IMAGE_PATH,
                             self.stub_os.readlink(self.system_image_link))
        self.stub_subprocess.AddCommand(side_effect=check_custom_image_link)

        provision.provision_device(
            self.platform, system_image_dir=self._CUSTOM_SYSTEM_IMAGE_DIR)

    def test_custom_system_image_missing(self):
        """Tests specifying a nonexistent custom system.img."""
        self.prepare_os_files()
        self.stub_os.path.should_exist.remove(self._CUSTOM_SYSTEM_IMAGE_PATH)
        self.stub_os.path.should_exist.append(self._DEFAULT_SYSTEM_IMAGE_PATH)
        self.stub_os.should_create_link.append((
            self._DEFAULT_SYSTEM_IMAGE_PATH,
            os.path.join(self.provision_temp_dir, 'system.img')))

        # In this case, even though the manifest may give a potential output
        # directory, the user has not built a custom system.img yet, so we
        # should be linking to the default system.img.
        def check_default_image_link():
            self.assertEqual(self._DEFAULT_SYSTEM_IMAGE_PATH,
                             self.stub_os.readlink(self.system_image_link))
        self.stub_subprocess.AddCommand(side_effect=check_default_image_link)

        provision.provision_device(
            self.platform, system_image_dir=self._CUSTOM_SYSTEM_IMAGE_DIR)

    def test_provision_exit_code(self):
        """Tests non-zero exit codes are propagated."""
        self.prepare_os_files()
        # Test an errno other than the default.
        errno = error.GENERIC_ERRNO + 1
        self.stub_subprocess.AddCommand(ret_code=errno)

        with self.assertRaises(tool.Error) as e:
            provision.provision_device(self.platform)
        self.assertEqual(errno, e.exception.errno)

    def test_fastboot_missing(self):
        """Tests failure when fastboot is missing."""
        self.prepare_os_files()
        self.stub_os.path.should_exist.remove(self._FASTBOOT_PATH)
        self.stub_subprocess.AddCommand()
        self.platform.should_link = False
        with self.assertRaises(provision.MissingBuildError):
            provision.provision_device(self.platform)

    def test_provision_device_missing(self):
        """Tests failure when provision-device is missing."""
        self.prepare_os_files()
        self.stub_os.path.should_exist.remove(self._PROVISION_DEVICE_PATH)
        self.stub_subprocess.AddCommand()
        self.platform.should_link = False

        with self.assertRaises(provision.MissingBuildError):
            provision.provision_device(self.platform)
