#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""core.user_config module stubs."""


from core import user_config


class StubUserConfig(object):
    """Stubs for our core.user_config module."""

    EXPOSED = user_config.EXPOSED

    def __init__(self, metrics_opt_in='0', os_root='/path/to/os'):
        self.USER_CONFIG.metrics_opt_in = metrics_opt_in
        self.USER_CONFIG.os_root = os_root

    class UserConfig(object):
        def __init__(self, file_path='user_data.db', table='totally_private'):
            self.file_path = file_path
            self.table = table
            self.metrics_opt_in = '0'
            self.uid = 'user_id'
            self.bsp_dir = '/somewhere/bsps'
            self.platform_cache = '/elsewhere/pc'
            self.is_complete = True
            self.os_root = '/where/the/tree/is'

        def complete(self):
            return self.is_complete

    USER_CONFIG = UserConfig()
