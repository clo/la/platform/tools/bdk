#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Wraps the per-product built host tool."""


import pprint

from cli import clicommand
from commands.product import constants
from core import config
from core import tool
from core import util


class Tool(clicommand.Command):
    """Run a host tool for a given product."""

    _TOOL_ENV_CMD = 'env'

    remainder_arg = ('args', 'Arguments to pass through to the tool')

    @staticmethod
    def Args(parser):
        parser.add_argument('tool',
                            choices=['adb', 'fastboot', Tool._TOOL_ENV_CMD],
                            help='Product host tool to execute')
        parser.add_argument('-p', '--product_path',
                            default=util.GetProductDir(),
                            help='Path to the root of the product')

    def Run(self, args):
        if args.product_path is None:
            print constants.MSG_NO_PRODUCT_PATH
            return 1

        store = config.ProductFileStore(args.product_path)
        t = tool.BrunchHostToolWrapper(store, args.product_path, args.tool)

        # Allow the fake toolenv command to pass through for introspection.
        if args.tool == self._TOOL_ENV_CMD:
            print 'Product path: {}'.format(args.product_path)
            print 'Tool path: {}'.format(t.path())
            print 'Tool environment:'
            pprint.pprint(t.environment)
            print 'Allowed passthrough environment: {}'.format(
                store.bdk.allowed_environ)
            return 0

        if not t.exists():
            print 'The product must be built once prior to using {}.'.format(
                args.tool)
            return 1

        t.run(args.args)
        return 0
