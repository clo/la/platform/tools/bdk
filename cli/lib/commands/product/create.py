#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Creates a product."""

import os

from cli import clicommand
from core import product
from core import util


class Create(clicommand.Command):
    """Create a product project in the current directory."""

    @staticmethod
    def Args(parser):
        parser.add_argument('name', help='Product name')
        # TODO(wad, arihc)(b/25670466) populate BSP choices here if possible.
        parser.add_argument('device', help='Device name (BSP)')
        parser.add_argument('-v', '--vendor', default='novendor',
                            help='Product vendor name')

    def Run(self, args):
        print 'Creating product "%s" . . .' % args.name

        os_path = util.DEPRECATED_GetDefaultOSPath()
        proj_path = os.path.abspath(args.name)
        if proj_path.startswith(os_path + os.path.sep):
            print 'Error: Products must be created outside of the OS tree.'
            return 1

        p = product.ProductCreator(args.name, args.vendor, args.device)
        if p.exists():
            print 'Product "%s" already exists' % args.name
            return 1
        p.create()
        return 0
