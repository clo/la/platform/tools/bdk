#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Wraps provision-device."""


import os

from cli import clicommand
from commands.product import constants
from core import config
from core import tool
from core import util
from project import platform


class Provision(clicommand.Command):
    """Run provision-device for a given product."""

    remainder_arg = ('args', 'Arguments to pass through to the tool')

    @staticmethod
    def Args(parser):
        parser.add_argument('-p', '--product_path',
                            default=util.GetProductDir(),
                            help='Path to the root of the product')
        # Workaround for https://bugs.python.org/issue17050
        # Need to specifically handle -s argument
        parser.add_argument('-s', metavar='<specific device>', default=None,
                            help='Provision a specific device')

    def Run(self, args):
        if args.product_path is None:
            print constants.MSG_NO_PRODUCT_PATH
            return 1

        store = config.ProductFileStore(args.product_path)

        # Get the platform so it can be linked. Build type required but unused.
        platform_ = platform.Platform(os_name='brillo', board=store.device,
                                      build_type=platform.BUILD_TYPE_ENG)

        t = tool.BrunchTargetToolWrapper(store, args.product_path,
                                         'provision-device')
        if not t.exists():
            print 'The product must be built once prior to using provision.'
            return 1

        # We have to export the host tools because provision-device
        # expects them in its path.
        ht = tool.BrunchHostToolWrapper(store, args.product_path, 'dummy')
        # Grab the tool path from the dummy tool.
        path_str = '%s%s' % (os.path.dirname(ht.path()), os.pathsep)
        if t.environment.has_key('PATH'):
            # The BrunchToolWrapper merges in the calling environment, so
            # we should always just be prefixing.
            t.environment['PATH'] = path_str + t.environment['PATH']
        else:
            print ('No PATH found in the environment! '
                   'Please set a PATH and call again.')
            return 1
        if args.s is not None:
            args.args += ['-s', args.s]
        with platform_.linked():
            t.run(args.args)
        return 0
