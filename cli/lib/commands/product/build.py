#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Builds a product."""


import os

from cli import clicommand
from commands.product import constants
from core import config
from core import tool
from core import util
from project import platform


class Build(clicommand.Command):
    """Build a product project from the current directory."""

    remainder_arg = ('make_args', 'Arguments to pass through to make.')

    @staticmethod
    def Args(parser):
        parser.add_argument('-s', '--submodule', action='store_true',
                            help='Build the submodule in the current '
                            'directory.')
        parser.add_argument('-p', '--product_path',
                            default=util.GetProductDir(),
                            required=False,
                            help='Path to the root of the product.')
        parser.add_argument('-b', '--buildtype', default=None, required=False,
                            choices=['eng', 'userdebug', 'user'],
                            help='Override the configured type of build to '
                            'perform.')

    def Run(self, args):
        sdk_path = util.GetBDKPath()

        if args.product_path is None:
            print constants.MSG_NO_PRODUCT_PATH
            return 1

        store = config.ProductFileStore(args.product_path)

        # Pull the buildtype from the config if it is not overridden.
        if args.buildtype is None:
            args.buildtype = store.bdk.buildtype

        # Get the platform so it can be linked.
        platform_ = platform.Platform(os_name='brillo', board=store.device,
                                      build_type=args.buildtype)

        no_java = 'BRILLO_NO_JAVA=0'
        if len(store.bdk.java) and store.bdk.java != '1':
            no_java = 'BRILLO_NO_JAVA=1'

        # This should only happen if the ProductFileStore is incomplete.
        if not args.buildtype:
            args.buildtype = 'userdebug'

        envcmd = 'make'

        make = tool.BrunchToolWrapper(store, args.product_path, 'make')
        wrap_makefile = os.path.join(sdk_path, 'build/wrap-product.mk')
        buildtype = 'BUILDTYPE=%s' % args.buildtype
        full_make_args = []
        if args.submodule == True:
            print "Building submodule only . . ."
            here = 'HERE=%s' % os.getcwd()
            full_make_args = ['-f', wrap_makefile,
                              no_java, here, buildtype,
                              'ENVCMD=mm'] + args.make_args
            envcmd = 'mm'
        else:
            # Change to the product path and build the whole product.
            make.set_cwd(args.product_path)
            full_make_args = ['-f', wrap_makefile,
                              no_java, buildtype] + args.make_args

        # Determine the envcmd for reporting purposes.
        for arg in args.make_args:
            split_arg = arg.split('=')
            if split_arg[0] == 'ENVCMD' and len(split_arg) > 1:
                envcmd = split_arg[1]
        self.SetMetricsLabel(envcmd)
        # TODO(arihc)(b/25952600): Some way to also prevent or at least filter
        #     metrics for build --help

        with platform_.linked():
            make.run(full_make_args)

        return 0
