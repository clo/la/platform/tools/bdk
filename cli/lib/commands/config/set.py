#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""Changes user config values."""


import os

from cli import clicommand
from core import user_config


class Set(clicommand.Command):
    """Set configuration values."""

    @staticmethod
    def Args(parser):
        parser.add_argument('option', choices=user_config.EXPOSED,
                            help='The variable to set.')
        parser.add_argument('value', help='The value to set the option to.')

    def Run(self, args):
        value = args.value
        if value and args.option in user_config.PATHS:
            # Make paths absolute if being set to something non-empty.
            value = os.path.abspath(value)
        setattr(user_config.USER_CONFIG, args.option, value)
        return 0
