#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""CLI command to flash an image onto an attached device."""


from cli import clicommand
from core import provision
from core import tool


class Flash(clicommand.Command):
    """Flashes the most recent build onto a device.

    If only one device is attached, it will be targeted by default. If
    more than one device is attached, a specific target can be passed
    with -s or by setting the ANDROID_SERIAL environment variable.
    """

    @staticmethod
    def Args(parser):
        Flash.AddProductArgs(parser)
        parser.add_argument('-s', metavar='<specific device>', default=None,
                            help='Target a specific device.')
        parser.add_argument('--no_reboot', dest='reboot', action='store_false',
                            help='Do not reboot after flashing.')

    def Run(self, args):
        """Runs the flash procedure.

        Args:
            args: parsed argparse namespace.

        Returns:
            0 if successful, an exit code otherwise.
        """
        spec, target = self.GetSpecAndTarget(args)
        platform = target.platform
        platform.verify_downloaded()

        system_image_dir = spec.config.output_dir

        # TODO(http://b/27503751): make sure the device exists and potentially
        # put it in fastboot mode if it's not.

        provision_args = ['-s', args.s] if args.s else []
        provision.provision_device(platform, system_image_dir, provision_args)

        if args.reboot:
            fastboot_tool = tool.HostToolWrapper(
                'fastboot', platform)
            fastboot_tool.run(provision_args + ['reboot'])

        return 0
