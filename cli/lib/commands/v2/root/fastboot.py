#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""CLI fastboot command."""


from __future__ import print_function

from cli import clicommand
from core import tool


class Fastboot(clicommand.Command):
    """Calls the fastboot executable."""

    remainder_arg = ('args', 'Arguments to pass through to fastboot.')

    @staticmethod
    def Args(parser):
        Fastboot.AddProductArgs(parser)

    def Run(self, args):
        """Runs fastboot.

        Args:
            args: parsed argparse namespace.

        Returns:
            0 if successful, an exit code otherwise.
        """
        _, target = self.GetSpecAndTarget(args)
        platform = target.platform

        fastboot_tool = tool.HostToolWrapper('fastboot', platform)
        if not fastboot_tool.exists():
            print('Could not find fastboot executable at {}. Use `bdk build '
                  'platform` to build it.'.format(fastboot_tool.path()))
            return 1

        fastboot_tool.run(args.args)
        return 0
