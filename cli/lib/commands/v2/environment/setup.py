#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""Sets up a sysroot."""

from __future__ import print_function
import os
import shutil

from bsp import device
from cli import clicommand
from core import util
from environment import sysroot
from environment import sysroot_util
from environment import toolchain_util


# Note: this should be kept in sync with project.platform.PLATFORM_CACHE_FORMAT
SAMPLE_MAKEFILE_TEMPLATE = """\
BDK={bdk_command}
DEVICE={board_namespace}
OS={os_namespace}

PLATFORM_CACHE:=$(shell $(BDK) config check platform_cache)/$(OS)/$(DEVICE)
SYSROOT=$(PLATFORM_CACHE)/sysroot
TOOLCHAIN=$(PLATFORM_CACHE)/toolchain
PKG_CONFIG_VARS=PKG_CONFIG_LIBDIR="$(SYSROOT)/usr/share/pkgconfig"\
 PKG_CONFIG_SYSROOT_DIR="$(SYSROOT)"

CXX=$(TOOLCHAIN)/g++
DEPS=
ifdef DEPS
CFLAGS=`$(PKG_CONFIG_VARS) pkg-config --cflags $(DEPS)`
LDFLAGS=`$(PKG_CONFIG_VARS) pkg-config --libs $(DEPS)`
endif

all: myproject

myproject: main.cpp
\t$(CXX) $(CFLAGS) $(LDFLAGS) --sysroot=$(SYSROOT) main.cpp -o myproject

clean:
\trm myproject
"""


class Setup(clicommand.Command):
    """Set up a sysroot in the given directory."""

    @staticmethod
    def Args(parser):
        Setup.AddProductArgs(parser)
        # TODO(b/28249627): This should be removed and replaced by some form
        #     of config.
        parser.add_argument('--pkgconfig_csv',
                            help='The path to a csv file describing packages '
                            'for the sysroot (default {}).'.format(
                                util.GetBDKPath('pkgconfig', 'packages.csv')),
                            default=util.GetBDKPath('pkgconfig',
                                                    'packages.csv'))

    def Run(self, args):
        """Sets up a sysroot and toolchain."""
        _, target = self.GetSpecAndTarget(args)
        platform = target.platform
        platform.verify_downloaded()

        # TODO(b/27385334): maybe toolchain/sysroot setup should really be
        #     different commands, with a helper command that invokes both.
        #     Alternatively, they could work together better such as by adding
        #     the necessary flags to use the sysroot into the tools.

        # Check args.
        if not os.path.isdir(platform.product_out_cache):
            print('Could not find platform build output "{}". '
                  'You must run\n'
                  '    bdk build platform\n'
                  'before you set up your build environment.'.format(
                      platform.product_out_cache))
            return 1
        if (platform.device.arch not in
            sysroot_util.SysrootUtil.DEFAULT_ARCH_INCLUDES):
            print('Unknown architecture: {}'.format(platform.device.arch))
            return 1
        if not os.path.isfile(args.pkgconfig_csv):
            print('No such file: {}'.format(args.pkgconfig_csv))
            return 1
        try:
            host_arch = util.GetHostArch()
        except util.HostUnsupportedArchError as e:
            print('Could not set up environment: {}'.format(e))
            return 1

        # Basic values.
        shared_libs = os.path.join(platform.product_out_cache, 'obj', 'lib')
        static_libs = os.path.join(platform.product_out_cache, 'obj',
                                   'STATIC_LIBRARIES')

        # TODO(b/27458309): determine an actual location for this.
        board_packages = platform.os.path('device',
                                          platform.device.vendor,
                                          platform.device.name,
                                          'developer',
                                          'packages.csv')

        ret = 0
        # Generate the sysroot.
        if os.path.exists(platform.sysroot):
            print ('Not generating sysroot: '
                   'something already exists at {}.'.format(platform.sysroot))
        else:
            sysroot_success = False
            brillo_sysroot = sysroot.Sysroot(platform.sysroot)
            setup_util = sysroot_util.SysrootUtil(brillo_sysroot, platform)
            # Setup from a base, add libs.
            try:
                setup_util.SetupBaseSysroot(platform.device.arch,
                                            shared_libs, static_libs)
                setup_util.AddSharedLibsFromCSV(shared_libs, args.pkgconfig_csv)
                # Add board libs, if any
                with platform.linked():
                    if os.path.isfile(board_packages):
                        setup_util.AddSharedLibsFromCSV(
                            shared_libs, board_packages,
                            suffix=platform.device.name)
                sysroot_success = True
            except (IOError, ValueError, device.Error) as e:
                print('Failed to generate sysroot: {}'.format(e))
            finally:
                if not sysroot_success:
                    ret = 1
                    brillo_sysroot.Destroy()

        # Generate toolchain.
        if os.path.exists(platform.toolchain):
            print('Not generating toolchain: '
                  'something already exists at {}.'.format(platform.toolchain))
        else:
            toolchain_success = False
            try:
                os.makedirs(platform.toolchain)
                toolchain_util.GenerateToolchain(platform, host_arch,
                                                 platform.toolchain)
                toolchain_success = True
            except toolchain_util.Error as e:
                print('Failed to generate toolchain: {}'.format(e))
            finally:
                if not toolchain_success:
                    ret = 1
                    shutil.rmtree(platform.toolchain)

        if not ret:
            print('Finished building environment.')
            print('Sample Makefile:\n\n' + SAMPLE_MAKEFILE_TEMPLATE.format(
                bdk_command=util.GetBDKPath('cli', 'bdk'),
                os_namespace=platform.os_namespace,
                board_namespace=platform.board_namespace))

        return ret
