#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""BDK CLI command classes."""


from core import timer
from core import util
import error
from metrics import metrics_util
from metrics.data_types import timing
from project import common
from project import project_spec


class Error(error.Error):
    pass


class SpecError(Error):
    """Raised when there is a problem with a product spec."""


class TargetError(Error):
    """"Raised when an invalid target is given."""


# pylint: disable=abstract-class-not-used
class Command(object):
    """A general cli command."""

    # Any subclass that wants to capture all remaining args should override
    # this with the desired arg (name, description) tuple. Do not use
    # argparse.REMAINDER directly as it has some unexpected behaviors.
    remainder_arg = None

    @staticmethod
    def Args(parser):
        pass

    @staticmethod
    def AddDefaultArgs(parser):
        parser.add_argument('--debug', action='store_true',
                            help='Output debug logging.')

    @staticmethod
    def AddProductArgs(parser, targeted=True):
        parser.add_argument('--specification', default=util.FindProjectSpec(),
                            help='The project specification file to read from. '
                            '(default {} in current or parent dirs)'.format(
                                util.PROJECT_SPEC_FILENAME))
        if targeted:
            parser.add_argument('--target',
                                help='The target to build the image of.')

    @classmethod
    def set_group(cls, grp):
        cls._group = grp

    @classmethod
    def set_parser(cls, parser):
        cls.parser = parser

    @classmethod
    def group(cls):
        try:
            return cls._group
        except AttributeError:
            return None

    def __init__(self):
        self._timer = timer.Timer(type(self).__name__)

    def GetSpecAndTarget(self, args):
        """Creates and verifies the spec and target_ to use.

        Args:
            args: parsed arg namespace; spec and target_ are based on
                args.specification and args.target.

        Returns:
            A tuple of (ProjectSpec, Target) objects.

        Raises:
            SpecError: If no project spec can be found.
            TargetError: If no target can be found.
            ValidationError: If the target fails to validate.
        """
        if not args.specification:
            raise SpecError(
                'No project spec could be found. Either run from a directory '
                'containing a project spec named "{}" (or run from within '
                'a subdirectory thereof), or specify a project spec file with '
                '`--specification`'.format(util.PROJECT_SPEC_FILENAME))
        try:
            spec = project_spec.ProjectSpec.from_xml(args.specification)
        except (IOError, common.LoadError) as e:
            raise SpecError('Error parsing product spec {}: {}'.format(
                args.specification, e))

        try:
            target_ = spec.get_target(args.target)
        except KeyError as e:
            raise TargetError(e)

        return (spec, target_)

    def SetMetricsClass(self, class_name):
        """Only necessary if you want to override the default metrics class.

        By default the class will be the command's class name.

        Args:
            class_name: The new name for metrics to send.
        """
        self._timer.name = class_name

    def SetMetricsLabel(self, label):
        """Add an optional label to the metrics being sent by this command."""
        self._timer.label = label

    def RunWithMetrics(self, args):
        """Wraps Run() with metrics collection and upload.

        This function times the call to Run(), and after completion
        uploads metrics (if the user has opted in) containing the
        command and timing data.

        Currently we upload metrics for all cases except KeyboardInterrupt,
        in order to make Ctrl+C as responsive as possible. If we get async
        metrics uploads going, it would probably be helpful to upload those
        as well (http://b/27703295).

        Args:
            args: list of args to pass to Run().

        Returns:
            Run() return value.

        Raises:
            Passes up any exceptions thrown by Run().
        """
        result = None
        try:
            self._timer.Start()
            result = self.Run(args)
        except KeyboardInterrupt:
            # Leave result as None to skip metrics upload for Ctrl+C.
            raise
        except error.Error as e:
            result = e.errno
            raise
        except:
            result = error.GENERIC_ERRNO
            raise
        finally:
            if result is not None:
                self._timer.Stop()
                metrics_util.send_hit_and_retries(
                    timing.Timing.from_timer(self._timer, result))

        return result

    def Run(self, args):
        """Called when the command is used.

        Args:
            args: An argparse.Namespace of arguments for the command.

        Returns:
            0 if successful, an exit code otherwise.
        """
        raise NotImplementedError('Run() must be overridden')


class Group(object):

    @staticmethod
    def GroupArgs(parser):
        """Set args that are available for all commands in a group."""
        pass
