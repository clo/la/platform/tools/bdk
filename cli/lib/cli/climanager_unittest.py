#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""Unit tests for climanager.py."""


import os
import unittest

import bdk
from cli import clicommand
from cli import climanager
from core import util_stub
from test import stubs
from test import test_util


# The test_cli package contains the necessary file structure to instantiate a
# Cli object with a few commands. See the test_cli package for test command
# implementation.
TEST_COMMAND_PACKAGE = os.path.join(os.path.dirname(__file__), 'test_cli')


class ParsingTest(unittest.TestCase):
    """Provides some common parse checking functionality."""

    # Each test must provide the command name and a cli object to parse.
    command = None
    cli = None

    # Test can provide a set of default kwargs to use if unspecified.
    defaults = {}

    def check_parse(self, input_args, **kwargs):
        """Parses args and verifies they match kwargs.

        Also checks that self.command matches the name of the CliCommand
        class that was selected.

        This may be called multiple times from a single test, it doesn't
        change any state.

        Args:
            input_args: list of string arguments to the parser.
            **kwargs: expected parsed namespace.

        Raises:
            On failure, raises unittest exceptions.
        """
        # pylint: disable=protected-access
        parsed = vars(self.cli._ParseArgs(input_args))

        # Make sure command_type is the correct class and that we added the
        # hidden remainder arg if necessary.
        command_type = parsed.pop('command_type')
        self.assertEqual(self.command, command_type.__name__.lower())
        if not command_type.remainder_arg:
            # The hidden remainder arg must not actually grab any args, it's
            # just there to improve error handling.
            self.assertIsNone(parsed.pop('_remainder_args'))

        for k, v in self.defaults.iteritems():
            kwargs.setdefault(k, v)
        self.assertDictEqual(kwargs, parsed)

    def check_parse_error(self, input_args, command, invalid_choice=None,
                          unknown_arguments=None, too_few_arguments=False):
        """Parses args and verifies an error is given.

        Callers may specify one of the keyword args to also verify that
        the error message prints out the indicated reason for failure.

        Args:
            input_args: list of string arguments to the parser.
            command: CLI command that should print the error.
            invalid_choice: an invalid choice error to check.
            unknown_arguments: list of unknown arguments to check.
            too_few_arguments: if True, checks for "too few arguments" error.

        Raises:
            On failure, raises unittest exceptions.
        """
        with test_util.OutputCapture() as output:
            with self.assertRaises(SystemExit):
                # pylint: disable=protected-access
                self.cli._ParseArgs(input_args)

        # Parsing errors should give a usage string as well.
        self.assertIn('usage:', output.stderr)
        self.assertRegexpMatches(output.stderr, '{}: error'.format(command))

        # Check for specific error messages. Some of the messages come from
        # argparse, which may turn out to be a little brittle if error strings
        # change between versions, but is useful for now as a sanity check that
        # we're producing the correct error message.
        error_text = None
        if invalid_choice:
            error_text = "invalid choice: '{}'".format(invalid_choice)
        elif unknown_arguments:
            error_text = 'unknown arguments: {}'.format(
                ' '.join(unknown_arguments))
        elif too_few_arguments:
            error_text = 'too few arguments'

        if error_text:
            self.assertRegexpMatches(output.stderr,
                                     'error: {}'.format(error_text))


class FakeCliTest(ParsingTest):
    """Tests argument parsing using our test CLI."""

    def setUp(self):
        self.cli = climanager.Cli('climanager_unittest_cli',
                                  TEST_COMMAND_PACKAGE)

    def check_parse(self, input_args, **kwargs):
        """Overrides check_parse() to add some helper functionality.

        For convenience, this override automatically prepends self.command
        so it doesn't have to be repeatedly specified in tests. It also runs
        the checker both with and without the --debug flag, unless debug
        was explicitly given in kwargs.

        These conveniences only work for the simple fake CLI with no
        subparsers, otherwise it becomes too complicated and error-prone
        to try to find the correct command and where to put --debug.

        Args:
            input_args: list of string arguments to the parser.
            **kwargs: expected parsed namespace.
        """
        check_func = super(FakeCliTest, self).check_parse
        if 'debug' in kwargs:
            check_func([self.command] + input_args, **kwargs)
        else:
            check_func([self.command] + input_args, debug=False, **kwargs)
            check_func([self.command, '--debug'] + input_args, debug=True,
                       **kwargs)


class PositionalOptionalTest(FakeCliTest):
    """Tests parsing the PositionalOptional command."""

    command = 'positionaloptional'

    def test_positional(self):
        self.check_parse(['foo'], positional='foo', optional=None)

    def test_both(self):
        for args in (['foo', '--optional', 'bar'],
                     ['--optional', 'bar', 'foo']):
            self.check_parse(args, positional='foo', optional='bar')

    def test_invalid(self):
        for args in ([],
                     ['foo', 'unknown_positional'],
                     ['foo', '--unknown_option']):
            with self.assertRaises(SystemExit):
                # pylint: disable=protected-access
                self.cli._ParseArgs(args)


class RemainderTest(FakeCliTest):
    """Tests parsing the Remainder command."""

    command = 'remainder'

    def test_empty(self):
        for args in ([], ['--']):
            self.check_parse(args, remainder=[])

    def test_multiple_separators(self):
        """Tests that only the first '--' is removed."""
        self.check_parse(['--', '--'], remainder=['--'])
        self.check_parse(['--', 'foo', '--'], remainder=['foo', '--'])

    def test_remainder(self):
        for args in (['foo'],
                     ['--bar'],
                     ['foo', '--bar'],
                     ['--bar', 'foo']):
            self.check_parse(args, remainder=args)
            self.check_parse(['--'] + args, remainder=args)

    def test_optional_overlap(self):
        """Tests parsing optional overlap in remainder."""
        # If an optional could match either the command or the remainder, it
        # goes to the command unless '--' is used to separate.
        self.check_parse(['--', '--debug'], remainder=['--debug'])

        # Once a remainder arg is found, the rest of the args are assumed to be
        # remainder even if they overlap with explicit options.
        for args in (['foo', '--debug'],
                     ['foo', '--debug', '--debug'],
                     ['--bar', '--debug'],
                     ['--bar', '--debug', '--debug']):
            self.check_parse(args, remainder=args)


class PositionalRemainderTest(FakeCliTest):
    """Tests parsing the PositionalRemainder command."""

    command = 'positionalremainder'

    def test_base(self):
        self.check_parse(['foo'], positional='foo', remainder=[])
        self.check_parse(['foo', 'bar'], positional='foo', remainder=['bar'])
        self.check_parse(['foo', '--baz'], positional='foo',
                         remainder=['--baz'])

    def test_invalid(self):
        for args in ([], ['--bar'], ['--bar', 'foo']):
            with self.assertRaises(SystemExit):
                self.check_parse(args)


class OptionalsRemainderTest(FakeCliTest):
    """Tests parsing the OptionalsRemainder command."""

    command = 'optionalsremainder'

    def test_base(self):
        self.check_parse(['foo'], flag_a=False, flag_b=False, option_x=None,
                         remainder=['foo'])

    def test_short_options(self):
        """Tests a few different ways to give short options."""
        for args in (['-a', '-b', '-x', 'X', 'foo'],
                     ['-ab', '-x=X', 'foo'],
                     ['-abxX', 'foo']):
            self.check_parse(args, flag_a=True, flag_b=True, option_x='X',
                             remainder=['foo'])

    def test_optional_overlap(self):
        """Tests that overlapping options stay in the remainder."""
        remainder = ['-b', '-x', 'Y', '-xZ', '-q']

        self.check_parse(['-abxX', '--'] + remainder,
                         flag_a=True, flag_b=True, option_x='X',
                         remainder=remainder)
        self.check_parse(['-abxX', 'foo'] + remainder,
                         flag_a=True, flag_b=True, option_x='X',
                         remainder=['foo'] + remainder)
        self.check_parse(['-abxX', '--bar'] + remainder,
                         flag_a=True, flag_b=True, option_x='X',
                         remainder=['--bar'] + remainder)


class BdkTest(ParsingTest):
    """Tests for the actual BDK CLI.

    These tests should only verify argument parsing, and should be
    kept somewhat minimal to avoid problems keeping them in sync with
    the CLI. Actual behavior/logic tests for the CLI should live next
    to the implementation instead.
    """

    _PROJECT_SPEC = 'bdk_test_project_spec.xml'

    defaults = {
        'debug': False,
        'specification': _PROJECT_SPEC,
        'target': None
    }

    def setUp(self):
        # Set BDK_V2_DEVELOPMENT environment var to enable the V2 commands.
        self.stub_os = stubs.StubOs()
        self.stub_os.environ['BDK_V2_DEVELOPMENT'] = 'true'

        self.stub_util = util_stub.StubUtil()
        self.stub_util.project_spec = self._PROJECT_SPEC

        clicommand.util = self.stub_util

        bdk.os = self.stub_os
        self.cli = bdk.GetBdkCli()


class BdkAdbTest(BdkTest):
    """Tests bdk adb parsing."""

    command = 'adb'

    def test_base(self):
        for passthrough in ([],
                            ['devices'],
                            ['-s', 'foo', 'shell']):
            self.check_parse(['root', 'adb'] + passthrough, args=passthrough)

    def test_debug(self):
        """Debug flag test.

        Tests when --debug applies to the bdk and when it's passed through.
        """
        self.check_parse(['root', 'adb', '--debug'], debug=True, args=[])
        self.check_parse(['root', 'adb', '--', '--debug'], args=['--debug'])
        self.check_parse(['root', 'adb', 'shell', '--debug'],
                         args=['shell', '--debug'])

        # --debug is currently an option of the final leaf commands, so trying
        # to specify it earlier is an error.
        self.check_parse_error(['--debug', 'root', 'adb'], 'bdk')
        self.check_parse_error(['root', '--debug', 'adb'], 'bdk root')


class BdkErrorTest(BdkTest):
    """Tests bdk parsing error handling."""

    def test_no_command(self):
        """Tests when no leaf command is specified."""
        self.check_parse_error([], 'bdk', too_few_arguments=True)
        self.check_parse_error(['root'], 'bdk root', too_few_arguments=True)

    def test_unknown_positional(self):
        """Tests when an unknown positional arg is given."""
        # When the arg is in the subparsers, expect "invalid choice" errors.
        for args in (['X'],
                     ['X', 'build'],
                     ['X', 'build', 'image']):
            self.check_parse_error(args, 'bdk', invalid_choice='X')

        for args in (['build', 'X'],
                     ['build', 'X', 'image']):
            self.check_parse_error(args, 'bdk build', invalid_choice='X')

        # When the arg is at a leaf command, expect an "unknown argument" error.
        self.check_parse_error(['build', 'image', 'X'], 'bdk build image',
                               unknown_arguments=['X'])

    def test_unknown_optional(self):
        """Tests when an unknown optional arg is given.

        In most cases we're able to successfully modify the parsing to make
        sure the correct subparser prints the error message, however one
        case still doesn't work:
            bdk --X build

        Ideally this should print a "bdk" error to match how the rest of
        the parsing works, i.e. stop as soon as we see an unknown optional.
        However, this particular situation errors out in the very first
        call to parse_known_args() and produces a "bdk build" error, so
        there's not much we can do outside of trying to parse manually.

        This doesn't happen if a full command is given, for example
        `bdk --X build image`, because in that case parse_known_args() is
        willing to skip over the unknown optional and we can do our custom
        error handling with the result.
        """
        # Ideally we'd get "unknown argument" errors here instead of "too few
        # arguments", but this isn't feasible while still printing the correct
        # subparser, which is the most important thing.
        for args in (['--X'],
                     # ['--X', 'build'] does not work here, see above.
                     ['--X', 'build', 'image'],
                     ['--X', 'build', 'platform']):
            self.check_parse_error(args, 'bdk', too_few_arguments=True)

        for args in (['build', '--X'],
                     ['build', '--X', 'image'],
                     ['build', '--X', 'platform']):
            self.check_parse_error(args, 'bdk build', too_few_arguments=True)

        self.check_parse_error(['build', 'image', '--X'], 'bdk build image',
                               unknown_arguments=['--X'])

        # Final sanity check to make sure 'build platform' does expect a
        # remainder, to make sure we're checking both a remainder and
        # non-remainder command.
        self.command = 'platform'
        self.check_parse(['build', 'platform', '--X'], args=['--X'])
