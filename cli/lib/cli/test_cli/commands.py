#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Test CLI commands."""


from cli import clicommand


class PositionalOptional(clicommand.Command):
    """A command with one positional and one optional arg."""

    @staticmethod
    def Args(parser):
        parser.add_argument('positional')
        parser.add_argument('--optional')

    def Run(self, _args):
        pass


class Remainder(clicommand.Command):
    """A command with remainder args."""

    remainder_arg = ('remainder', 'help text')

    def Run(self, _args):
        pass


class PositionalRemainder(clicommand.Command):
    """A command with one positional and remainder args."""

    remainder_arg = ('remainder', 'help text')

    @staticmethod
    def Args(parser):
        parser.add_argument('positional')

    def Run(self, _args):
        pass


class OptionalsRemainder(clicommand.Command):
    """A command with multiple optionals and a remainder."""

    remainder_arg = ('remainder', 'help text')

    @staticmethod
    def Args(parser):
        parser.add_argument('-a', '--flag_a', action='store_true')
        parser.add_argument('-b', '--flag_b', action='store_true')
        parser.add_argument('-x', '--option_x')

    def Run(self, _args):
        pass
