#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""Unit tests for clicommand.py."""


import unittest

from cli import clicommand
from core import user_config_stub
from metrics import hit_store_stub
from metrics import metrics_util
from metrics.data_types import hit_stub


def _keyboard_interrupt_func(_args):
    """Helper function to raise KeyboardInterrupt."""
    raise KeyboardInterrupt()


def _base_exception_func(_args):
    """Helper function to raise BaseException."""
    raise BaseException()


class RunWithMetricsTest(unittest.TestCase):
    """Tests to make sure RunWithMetrics() behaves as expected."""

    def setUp(self):
        self.user_config_stub = user_config_stub.StubUserConfig(
            metrics_opt_in='1')
        self.hit_stub = hit_stub.StubHit()
        self.hit_store_stub = hit_store_stub.StubHitStore()
        self.user_store_stub = self.user_config_stub.USER_CONFIG

        clicommand.metrics_util.user_config = self.user_config_stub
        clicommand.metrics_util.hit = self.hit_stub
        clicommand.metrics_util.hit_store = self.hit_store_stub
        # pylint: disable=protected-access
        clicommand.metrics_util._MetricsUtilState.user_store = (
            self.user_store_stub)

        self.command = clicommand.Command()

    def last_uploaded_result(self):
        """Helper to return the most recent uploaded result or None."""
        if hit_stub.StubHit.last_send is None:
            return None
        return hit_stub.StubHit.get_custom_data_field(
            # pylint: disable=protected-access
            metrics_util._BRILLO_CD_RESULT)

    def test_upload_result(self):
        """Tests successfully uploading a Run() result."""
        for result in (0, 1, 130):
            self.command.Run = lambda _args, ret=result: ret

            self.assertEqual(result, self.command.RunWithMetrics([]))
            self.assertEqual(result, self.last_uploaded_result())

    def test_keyboard_interrupt(self):
        """Tests metrics are not uploaded after Ctrl+C."""
        self.command.Run = _keyboard_interrupt_func

        with self.assertRaises(KeyboardInterrupt):
            self.command.RunWithMetrics([])
        self.assertIsNone(self.last_uploaded_result())

    def test_base_exception(self):
        """Tests metrics are uploaded for any other exception."""
        self.command.Run = _base_exception_func

        with self.assertRaises(BaseException):
            self.command.RunWithMetrics([])
        self.assertEqual(1, self.last_uploaded_result())

    def test_metrics_opt_out(self):
        """Tests metrics are never sent if the user has opted out."""
        self.user_config_stub.USER_CONFIG.metrics_opt_in = '0'

        self.command.Run = lambda _args: 0
        self.command.RunWithMetrics([])
        self.assertIsNone(self.last_uploaded_result())

        self.command.Run = lambda _args: 1
        self.command.RunWithMetrics([])
        self.assertIsNone(self.last_uploaded_result())

        self.command.Run = _keyboard_interrupt_func
        with self.assertRaises(KeyboardInterrupt):
            self.command.RunWithMetrics([])
        self.assertIsNone(self.last_uploaded_result())

        self.command.Run = _base_exception_func
        with self.assertRaises(BaseException):
            self.command.RunWithMetrics([])
        self.assertIsNone(self.last_uploaded_result())
