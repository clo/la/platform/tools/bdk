#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""BDK CLI top-level entry point."""


from __future__ import print_function
import os
import sys

# pylint: disable=relative-import
from cli import climanager
import error
from metrics import metrics_util
from metrics.data_types import exception


def CommandPath(name):
    bdk_lib_path = os.path.dirname(__file__)
    path = os.path.join(bdk_lib_path, 'commands', name)
    return path


def GetBdkCli():
    """Creates the fully populated BDK CLI.

    Returns:
        The climanager.Cli object.
    """
    # Create the CLI.
    bdk_cli = climanager.Cli('bdk', CommandPath('root'))

    # Add command groups.
    bdk_cli.AddCommandGroup('bsp', CommandPath('bsp'))
    bdk_cli.AddCommandGroup('config', CommandPath('config'))
    bdk_cli.AddCommandGroup('product', CommandPath('product'))

    if 'BDK_V2_DEVELOPMENT' in os.environ:
        bdk_cli.AddCommandGroup('build', CommandPath('v2/build'))
        bdk_cli.AddCommandGroup('environment', CommandPath('v2/environment'))
        bdk_cli.AddCommandGroup('root', CommandPath('v2/root'))

    return bdk_cli


def main():
    # Initialize metrics.
    metrics_util.initialize()

    bdk_cli = GetBdkCli()
    try:
        return bdk_cli.Execute()
    # pylint: disable=broad-except
    except Exception as e:
        metrics_util.send_hit_and_retries(
            exception.ExceptionHit.from_exception(e, is_fatal=True))
        if bdk_cli.args.debug:
            raise
        else:
            print(e, file=sys.stderr)
            if isinstance(e, error.Error):
                return e.errno
            return 1


if __name__ == '__main__':
    try:
        sys.exit(main())
    except KeyboardInterrupt:
        # Handle Ctrl+C so we don't print stack traces.
        sys.stderr.write('bdk killed by user\n')
        sys.exit(1)
