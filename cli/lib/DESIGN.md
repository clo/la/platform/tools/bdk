Bdk Design.

* Source layout:
cli/
  lib/            # python top level package
    cli/          # Cli package
    commands/     # Sub command logic
    core/         # package for general tools and setup logic
    productspec/  # package for product spec logic
    metrics/      # package for metrics
    bsp/          # package for bsp downloading
    product/      # package for product managment
    bdk.py     # main python entry point
  bdk          # shell script entry point

* CLI implementation
TODO(leecam)(b/25952505): Write this up
