#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Tests for file_context.py"""


import stat
import unittest

from selinux import file_context


class ModeMapTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_mode_to_typename(self):
        seen = []
        for mode in [stat.S_IFSOCK, stat.S_IFLNK, stat.S_IFREG, stat.S_IFBLK,
                     stat.S_IFDIR, stat.S_IFCHR, stat.S_IFIFO]:
            ft = file_context.mode_to_file_type(mode)
            # Matched
            self.assertNotEqual(ft, 'ALL')
            # Not returned before (no accidental dupes).
            self.assertNotIn(ft, seen)
            seen.append(ft)
