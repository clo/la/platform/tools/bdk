#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""A class for hits of the "timing" type."""


from metrics import metrics_util
from metrics.data_types import hit


class Timing(hit.Hit):
    """The Timing class can be used to track timings.

    An Timing provides several fields that might be useful for tracking timings.
    What each field should represent is left to the user.

    Attributes:
        category: the Timing category.
        variable: the Timing variable.
        time: the numeric duration timed, in ms.
        label: (optional) a label for the Timing.
        other attributes inherited from base class.
    """

    # GA field keys.
    GA_KEY_USER_TIMING_CATEGORY = 'utc'
    GA_KEY_USER_TIMING_VARIABLE = 'utv'
    GA_KEY_USER_TIMING_TIME = 'utt'
    GA_KEY_USER_TIMING_LABEL = 'utl'

    def __init__(self, meta_data, category, variable, time,
                 label=None, custom_dimensions=None, custom_metrics=None):
        super(Timing, self).__init__(meta_data,
                                     self.GA_TIMING_TYPE,
                                     custom_dimensions,
                                     custom_metrics)
        self.category = category
        self.variable = variable
        self.time = time
        self.label = label

    def get_fields(self):
        """See base class."""
        params = super(Timing, self).get_fields()
        params.update({self.GA_KEY_USER_TIMING_CATEGORY: self.category,
                       self.GA_KEY_USER_TIMING_VARIABLE: self.variable,
                       self.GA_KEY_USER_TIMING_TIME: self.time})
        if self.label:
            params[self.GA_KEY_USER_TIMING_LABEL] = self.label
        return params

    @classmethod
    def from_timer(cls, timer, result=None):
        """Converts a timer.Timer object to a Timing hit.

        The hit has the following structure:
            User Timing Category: timer.name
            User Timing Variable: Timer reading in s, rounded to nearest.
            User Timing Time: Timer reading in ms, rounded to nearest.
            User Timing Label: timer.label
            Custom Dimension Result: result arg, if any.
            Other custom dimensions and metadata: as defined in metrics_util.

        The time is as Time in ms because that's what GA expects. We also
        send it as the Variable in s so we can construct histograms.
        ("Time" is a metric, meaning it gets inseparably conglomerated,
        while "Variable" is a dimension, meaning we can sort data by it.
        We use seconds to reduce the number of distinct rows to make
        processing easier.)
        """
        current_time = timer.Read()
        return cls(metrics_util.get_meta_data(),
                   timer.name,
                   int(round(current_time)),
                   int(round(current_time * 1000)),
                   timer.label,
                   metrics_util.get_custom_dimensions(result=result))
