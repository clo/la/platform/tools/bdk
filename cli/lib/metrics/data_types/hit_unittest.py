#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Unittests for hit.py."""


import subprocess
import unittest

from metrics.data_types import hit
from metrics.data_types import meta_data
from test import stubs


class HitTest(unittest.TestCase):
    """Hit unittest class."""

    def setup_default_hit_fields(self):
        """Set up the default metadata, custom dimensions, and metrics.

        Attributes:
            data: A meta_data.MetaData object.
            custom_dimensions: A dictionary of custom dimensions.
            custom_metrics: A dictionary of custom metrics.
            expected_fields: A dictionary of the expected fields.
        """
        self.expected_fields = {}

        self.data = meta_data.MetaData(100,  # Protocol Version
                                       'APP_ID_1',  # App ID
                                       'app_name_1',  # App name
                                       'app_version_2',  # App version
                                       'abcdef01234'  # User ID
                                      )
        self.expected_fields.update({
            hit.Hit.GA_KEY_PROTOCOL_VERSION: 100,
            hit.Hit.GA_KEY_TRACKING_ID: 'APP_ID_1',
            hit.Hit.GA_KEY_APPLICATION_NAME: 'app_name_1',
            hit.Hit.GA_KEY_APPLICATION_VERSION: 'app_version_2',
            hit.Hit.GA_KEY_CLIENT_ID: 'abcdef01234'
        })

        self.custom_dimensions = {x: 'dimension{}'.format(x)
                                  for x in range(1, 8)}
        self.expected_fields.update({
            '{}{}'.format(hit.Hit.GA_KEY_CUSTOM_DIMENSION, x):
                'dimension{}'.format(x) for x in range(1, 8)
        })

        self.custom_metrics = {x: 'metrics{}'.format(x) for x in range(1, 8)}
        self.expected_fields.update({
            '{}{}'.format(hit.Hit.GA_KEY_CUSTOM_METRIC, x):
                'metrics{}'.format(x) for x in range(1, 8)
        })

    def setup_hit(self):
        """Set up the fields for testing.

        Attributes:
            hit: The hit object under test.
            expected_fields: The expected fields sent by the hit.
        """
        self.setup_default_hit_fields()

        self.hit = hit.Hit(self.data,
                           'my_hit_type',
                           custom_dimensions=self.custom_dimensions,
                           custom_metrics=self.custom_metrics)

        self.expected_fields.update({hit.Hit.GA_KEY_HIT_TYPE: 'my_hit_type'})

    def setUp(self):
        """Set up the test."""
        self.stub_os = stubs.StubOs()
        self.stub_open = stubs.StubOpen(self.stub_os)
        self.stub_subprocess = stubs.StubSubprocess(stub_os=self.stub_os,
                                                    stub_open=self.stub_open)
        self.stub_urllib = stubs.StubUrllib()

        hit.popen.subprocess = self.stub_subprocess
        hit.urllib = self.stub_urllib

        self.data = None
        self.custom_dimensions = {}
        self.custom_metrics = {}
        self.hit = None
        self.expected_fields = {}

        self.setup_hit()

    def test_get_fields(self):
        """Test that hit.get_fields() returns the expected fields."""
        fields = self.hit.get_fields()
        self.assertEqual(fields, self.expected_fields)

    def test_send_fields(self):
        """Test that hit.Hit.send_fields(fields) correctly sends the fields."""
        subproc = self.stub_subprocess.AddCommand()
        hit.Hit.send_fields(self.hit.get_fields())
        subproc.AssertCallWas(
            ['curl', '--data', self.stub_urllib.urlencode(self.expected_fields),
             hit.Hit._GA_ENDPOINT],  # pylint: disable=protected-access
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
