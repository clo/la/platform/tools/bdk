#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Unittests for timing.py"""


from core import timer
from metrics import metrics_util_stub
from metrics.data_types import hit_unittest
from metrics.data_types import timing
from test import stubs


class TimingTest(hit_unittest.HitTest):
    """Timing unittest class."""

    def setup_hit(self):
        """Set up the fields for testing.

        Attributes:
            hit: The hit object under test.
            expected_fields: The expected fields sent by the hit.
        """
        self.setup_default_hit_fields()

        self.hit = timing.Timing(self.data,
                                 'my_category',
                                 'my_variable',
                                 200,
                                 label='my_label',
                                 custom_dimensions=self.custom_dimensions,
                                 custom_metrics=self.custom_metrics)

        self.expected_fields.update({
            timing.Timing.GA_KEY_HIT_TYPE: 'timing',
            timing.Timing.GA_KEY_USER_TIMING_CATEGORY: 'my_category',
            timing.Timing.GA_KEY_USER_TIMING_VARIABLE: 'my_variable',
            timing.Timing.GA_KEY_USER_TIMING_TIME: 200,
            timing.Timing.GA_KEY_USER_TIMING_LABEL: 'my_label'
        })

    def setUp(self):
        super(TimingTest, self).setUp()

        self.stub_time = stubs.StubTime()

        timer.time = self.stub_time

    def test_from_timer(self):
        """Tests the timing.Timing.from_timer(..) method."""
        metrics_util_stub_ = metrics_util_stub.StubMetricsUtil(self.data, {})
        timing.metrics_util = metrics_util_stub_

        my_timer = timer.Timer('test_timer')
        my_timer.name = 'test_name'
        my_timer.label = 'test_label'
        my_timer.Start()
        self.stub_time.current_time = 12.3
        my_timer.Stop()

        hit = timing.Timing.from_timer(my_timer)
        self.assertEqual(hit.category, 'test_name')
        self.assertEqual(hit.variable, 12)  # Variable is in s.
        self.assertEqual(hit.time, 12300)  # Time is in ms.
        self.assertEqual(hit.label, 'test_label')
