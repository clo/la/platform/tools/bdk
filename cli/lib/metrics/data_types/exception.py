#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""A class for sending exception type hits to GA."""


from metrics import metrics_util
from metrics.data_types import hit


class ExceptionHit(hit.Hit):
    """The ExceptionHit class can be used to send exception hits.

    Exceptions are one of the base types for Google Analytics hits, this
    class can be used to send a Python exception.  The built-in fields that
    are supported are fairly brief: just a short 150 Byte description of the
    exception, and a boolean for whether or not it was a fatal exception.

    Attributes:
        exception_desc: The description of the exception, typically the
            exception type name.
        is_fatal: If the exception is fatal.
    """

    # GA field keys.
    GA_KEY_EXCEPTION_DESCRIPTION = 'exd'
    GA_KEY_EXCEPTION_FATAL = 'exf'

    def __init__(self, meta_data, exception_desc, is_fatal=False,
                 custom_dimensions=None, custom_metrics=None):
        super(ExceptionHit, self).__init__(meta_data,
                                           self.GA_EXCEPTION_TYPE,
                                           custom_dimensions,
                                           custom_metrics)
        self.exception_desc = exception_desc
        self.is_fatal = is_fatal

    def get_fields(self):
        """See base class."""
        params = super(ExceptionHit, self).get_fields()
        params.update({self.GA_KEY_EXCEPTION_DESCRIPTION: self.exception_desc,
                       self.GA_KEY_EXCEPTION_FATAL: self.is_fatal})
        return params

    @classmethod
    def from_exception(cls, exception, is_fatal=False):
        """Converts an Exception object to an ExceptionHit"""
        return cls(metrics_util.get_meta_data(),
                   type(exception).__name__,
                   is_fatal=is_fatal,
                   custom_dimensions=metrics_util.get_custom_dimensions())
