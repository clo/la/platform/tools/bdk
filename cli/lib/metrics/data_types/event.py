#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""A class for hits of the "event" type."""

from metrics.data_types import hit


class Event(hit.Hit):
    """The Event class can be used to track events.

    An Event provides several fields that might be useful for tracking events.
    What each field should represent is left to the user.

    Attributes:
        category: the Event category.
        action: the Event action.
        value: (optional) a numeric value for the Event.
        label: (optional) a label for the Event.
        other attributes inherited from base class.
    """

    # GA field keys.
    GA_KEY_EVENT_CATEGORY = 'ec'
    GA_KEY_EVENT_ACTION = 'ea'
    GA_KEY_EVENT_LABEL = 'el'
    GA_KEY_EVENT_VALUE = 'ev'

    def __init__(self, meta_data, category, action,
                 value=None, label=None,
                 custom_dimensions=None, custom_metrics=None):
        super(Event, self).__init__(meta_data,
                                    self.GA_EVENT_TYPE,
                                    custom_dimensions,
                                    custom_metrics)
        self.category = category
        self.action = action
        self.label = label
        self.value = value

    def get_fields(self):
        """See base class."""
        params = super(Event, self).get_fields()
        params.update({self.GA_KEY_EVENT_CATEGORY: self.category,
                       self.GA_KEY_EVENT_ACTION: self.action})
        if self.label:
            params[self.GA_KEY_EVENT_LABEL] = self.label
        if self.value:
            params[self.GA_KEY_EVENT_VALUE] = self.value
        return params
