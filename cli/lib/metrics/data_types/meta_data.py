#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""A MetaData class for Google Analytics hits.

MetaData abstracts the required fields of a hit that do not directly
relate to the details of the hit.

Attributes:
  version: the Analytics Measurement Protocol version being used.
  app_id: the ID of the GA property to send data to.
  app_version: The application version (i.e. BDK version).
  user_id: an ID unique to a particular user.
"""

import collections

MetaData = collections.namedtuple('MetaData', ('version', 'app_id', 'app_name',
                                               'app_version', 'user_id'))
