#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""A stub for the Target class."""


class StubTarget(object):

    # TODO(b/28296932): Rename the 'os' input variable to not conflict with the
    # import.
    # pylint: disable=redefined-outer-name
    def __init__(self, name='',
                 origin='test_target_origin',
                 platform=None,
                 submap_raises=None, submaps=None):
        self.name = name
        self.origin = origin
        self.platform = platform

        self._submap_raises = submap_raises
        self._submaps = submaps or []

    def create_submap(self, _packmap):
        if self._submap_raises:
            # Pylint issues an error about raising NoneType, since
            # self._submap_raises is set to None by default and pylint doesn't
            # recognize that the raise call is only called if it is not None.
            # In the case that it is set to a non-Exception, running the unit
            # test should raise a TypeError and (correctly) fail the unit test,
            # so it should be safe to just disable the pylint check here.
            # pylint: disable=raising-bad-type
            raise self._submap_raises
        return self._submaps.pop(0)
