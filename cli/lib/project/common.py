#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Helpers and exceptions for parsing classes."""

import copy
import os

import error


pathsep = '/'


# Attempt to convert common.pathsep to the host os for all
# source files.
# TODO(wad,dpursell): Sort out how absolute paths work in this world.
def path_to_host(path):
    return path.replace(os.sep, pathsep)

def path_join(*args):
    """Naive path join to use the module-local pathsep."""
    path = ''
    for c in args:
        if (path is not '' and
                not path.endswith(pathsep) and
                not c.startswith(pathsep)):
            path += pathsep
        path += c
    return path

def basename(path):
    return path.split(pathsep)[-1]


class Origin(object):
    """Used to track the origin location of XML tags."""
    def __init__(self, f='<unknown>', line='-', col='-'):
        self.source_file = f
        self.line_number = line
        self.column_number = col

    def __eq__(self, other):
        return (
            isinstance(other, self.__class__) and
            other.source_file == self.source_file and
            other.line_number == self.line_number and
            other.column_number == self.column_number
        )

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return '{}:{}:{}'.format(
            self.source_file, self.line_number, self.column_number)

    def copy(self):
        """Provide a dedicated helper for copying to encourage
        deep copying to avoid keeping references to XML tree
        data.
        """
        return copy.deepcopy(self)


class Error(error.Error):
    pass


class LoadError(Error):
    pass


class LoadErrorWithOrigin(LoadError):
    def __init__(self, origin, message='', errno=error.GENERIC_ERRNO):
        # Prefix where the error occurred at.
        message = '{}: {}'.format(origin, message)
        super(LoadErrorWithOrigin, self).__init__(message, errno=errno)

class UnknownAttributes(LoadErrorWithOrigin):
    pass


class MissingAttribute(LoadErrorWithOrigin):
    pass


class PathConflictError(LoadErrorWithOrigin):
    pass
