#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Classes representing dependency information."""

from project import common


class Error(common.Error):
    """The base class for any dependency-related error."""


class UnsatisfiedVirtualPackError(Error):
    """Raised when a dependency could be resolved globally, but isn't for
         the given dependency tree.
    """
    def __init__(self, deps):
        # TODO(wad) Sort by origin
        msg = '\n'.join([str(d) for d in deps])
        super(UnsatisfiedVirtualPackError, self).__init__(msg)


class UndefinedPackError(Error):
    def __init__(self, undef):
        # TODO(wad) Sort by origin
        msg = '\n'.join([str(d) for d in undef])
        super(UndefinedPackError, self).__init__(msg)


class Dependency(object):
    pass


class Virtual(Dependency):
    def __init__(self, name, root, required_by, candidates):
        self._name = name
        self._root = root
        self._required_by = required_by
        self._candidates = candidates

    def __repr__(self):
        return ('Pack(s) {} requires virtual pack "{}" is unsatisfied in '
                'the dependency tree for pack "{}". The following packs '
                'may satisfy the requirement: {}'.format(
                    ', '.join(['"{}"'.format(r) for r in self._required_by]),
                    self._name, self._root, self._candidates))


class Undefined(Dependency):
    def __init__(self, name, required_by):
        self._name = name
        self._required_by = required_by

    def __repr__(self):
        s = 'Undefined pack "{}" is required by '.format(self._name)
        callers = []
        for p in self._required_by:
            callers += ['pack "{}" defined at "{}"'.format(p.uid, p.origin)]
        s += '{}.'.format(', '.join(callers))
        return s
