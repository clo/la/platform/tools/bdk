#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""A stub for the PackMap class."""


class StubPackMap(object):

    def __init__(self, pack_map=None, provides=None, missing=None, origins=None,
                 packs=None, destinations=None):
        self.map = pack_map or {}
        self.provides = provides or {}
        self.missing = missing or {}
        self.origins = origins or {}
        self.copy_destinations = destinations or {}
        self.packs = packs or []
