#
# Copyright (C) 2016 The Android Open Source ProjectSpec
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
    This file is the parsing entry point for BDK XML project.

    BDK XML configuration is specified in the schemas/ path and
    all input files should be verifiable with a RELAX-NG verifier
    like 'jing'.

    At present, this entry point only performs content validation
    and not formal syntax validation.
"""


import os

from project import common
from project import config
from project import packmap
from project import packs
from project import targets
from project import xml_parser


class ProjectSpec(object):
    """ProjectSpec represents the primary interface to a user-defined project.

         The project is created from a BDK project XML file (defined by
         the bdk.rng RELAX NG schema).
    """
    def __init__(self):
        self._version = 1
        self._targets = {}
        self._config = config.Config()
        self._packmap = packmap.PackMap()
        self._origin = common.Origin()

    @property
    def config(self):
        """Returns the Config object."""
        return self._config

    @config.setter
    def config(self, config_):
        self._config = config_

    @property
    def packmap(self):
        """Returns the PackMap object."""
        return self._packmap

    def add_target(self, target):
        if target.name in self._targets:
            raise common.LoadErrorWithOrigin(
                target.origin,
                ('Target "{}" redefined. Previously definition here: '
                 '"{}"'.format(target.name, self._targets[target.name].origin)))
        self._targets[target.name] = target

    def add_packs(self, packs_obj):
        self._packmap.update(packs_obj)

    @property
    def targets(self):
        """Returns a dict of Targets keyed by name."""
        return self._targets

    @property
    def origin(self):
        return self._origin

    @origin.setter
    def origin(self, o):
        self._origin = o.copy()

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, v):
        self._version = v

    def __repr__(self):
        return ('<project version="{}" origins="{}">'
                '{}{}{}</project>').format(
                    self._version, self.packmap.origins, self._config,
                    self.packmap.packs, self.targets)

    @classmethod
    def from_xml(cls, src='project.xml'):
        """Populates this instance with the ProjectSpec from file.

        To validate the resulting ProjectSpec,
            config.packmap.report_missing()
        may be called to identify globally undefined packs.

        And config.targets[t].create_submap(config.packmap)
        must be called to identify unsatisfied dependencies or undefined
        pack names per-target.

        Args:
            src: If str, a path to XML file defining a <project>.
                 If file compatible, the XML file.

        Returns:
            instance of ProjectSpec

        Raises:
            IOError
            common.LoadError or ones of its subclasses:
                common.LoadErrorWithOrigin
                common.PathConflictError
                packmap.UpdateError
        """
        project_spec = cls()
        if isinstance(src, basestring):
            src = os.path.abspath(src)
        tree = xml_parser.parse(src)
        root = tree.getroot()
        # pylint: disable=no-member
        project_spec.origin = root.origin
        root.limit_attribs(['version'])
        # Ensure we have a project document.
        if root.tag != 'project':
            # pylint: disable=no-member
            raise common.LoadErrorWithOrigin(root.origin,
                                             'root node not <project>')

        if 'version' in root.attrib:
            project_spec.version = root.attrib['version']

        # Pull in each top level element.
        cfg = root.findall('config')
        if len(cfg) > 1:
            raise common.LoadErrorWithOrigin(
                cfg[1].origin, 'Only one <config> element may be defined.')
        if len(cfg):
            project_spec.config = config.Config.from_element(cfg[0])
        cls.collect_packs(root, project_spec.add_packs)
        cls.collect_targets(root, project_spec.packmap, project_spec.add_target)
        if (project_spec.config.default_target and
                project_spec.config.default_target not in project_spec.targets):
            # TODO(wad) The origin could be made more specific we keep the
            #     origins per-child around.
            raise common.LoadErrorWithOrigin(
                project_spec.config.origin,
                'default target "{}" is not defined.'.format(
                    project_spec.config.default_target))
        # If there's only one target and no default, set the default to that
        # one.
        if (not project_spec.config.default_target and
                len(project_spec.targets)) == 1:
            project_spec.config.default_target = project_spec.targets.keys()[0]
        return project_spec

    @staticmethod
    def collect_packs(root_node, callback):
        """Collects all <Packs> passing them to callback(packs)."""
        for node in root_node.findall('packs'):
            callback(packs.PacksFactory().new(element=node))

    @staticmethod
    def collect_targets(root, packmap_, callback):
        """Collects all <target> from <targets> passing them to
        callback(target).
        """
        for node in root.findall('targets'):
            t = targets.TargetsFactory.new(packmap_, element=node)
            for tgt in t.targets.values():
                callback(tgt)

    def get_target(self, target_name=None):
        """Gets a target by name, or the default target if none is specified.

        Raises:
            KeyError: if the spec does not have the requested target,
                or the default target is requested but not defined.
        """
        # Try to find a default target if not specified.
        if not target_name:
            if self.config.default_target:
                target_name = self.config.default_target
            else:
                raise KeyError('{}: No default target could be found '
                               'in project spec. Possible targets are: '
                               '{}.'.format(self.origin, self.targets.keys()))

        # Check that the desired target exists.
        if target_name not in self.targets:
            raise KeyError('{}: No such target "{}" (options are: {})'.format(
                self.origin, target_name, self.targets.keys()))

        return self.targets[target_name]
