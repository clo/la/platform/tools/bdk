#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""Unit tests for platform.py."""


import unittest

from bsp import device_stub
from bsp import manifest_stub
from bsp import operating_system_stub
from core import user_config_stub
from core import util_stub
from project import platform
from test import stubs


class PlatformTest(unittest.TestCase):

    _OS = 'brillo'
    _OS_VERSION = '99.11.00'
    _BSP = 'this_is_a_board'
    _BSP_VERSION = '80.80'
    _BUILD_TYPE = 'userdebug'
    # Should not match any other version used above.
    _BAD_VERSION = '0.0'

    def setUp(self):
        self.stub_op_sys = operating_system_stub.StubOperatingSystemModule(
            os_name=self._OS, os_version=self._OS_VERSION, os_available=True)
        self.stub_manifest = manifest_stub.StubManifestModule(self.stub_op_sys)
        self.stub_os = stubs.StubOs()
        self.stub_user_config = user_config_stub.StubUserConfig()
        self.stub_util = util_stub.StubUtil()

        platform.manifest = self.stub_manifest
        platform.os = self.stub_os
        platform.user_config = self.stub_user_config
        platform.util = self.stub_util

        self.dev = device_stub.StubDevice(self._BSP, version=self._BSP_VERSION,
                                          downloaded=True)
        self.stub_manifest.Manifest.from_json_devices = {self._BSP: self.dev}
        self.platform = platform.Platform(
            os_name=self._OS, os_version=self._OS_VERSION, board=self._BSP,
            board_version=self._BSP_VERSION, build_type=self._BUILD_TYPE)

    def test_init_valid(self):
        # Check all the properties of our correctly initialized platform.

        self.assertEqual(self.platform.os.name, self._OS)
        self.assertEqual(self.platform.os.version, self._OS_VERSION)
        self.assertEqual(self.platform.build_type, self._BUILD_TYPE)
        self.assertEqual(self.platform.device, self.dev)
        self.assertEqual(self.platform.device.name, self._BSP)
        self.assertEqual(self.platform.device.version, self._BSP_VERSION)
        # Namespaces should somehow be related to names & versions.
        self.assertIn(self._OS, self.platform.os_namespace)
        self.assertIn(self._OS_VERSION, self.platform.os_namespace)
        self.assertIn(self._BSP, self.platform.board_namespace)
        self.assertIn(self._BSP_VERSION, self.platform.board_namespace)
        cache_root = self.stub_os.path.join(
            self.stub_user_config.USER_CONFIG.platform_cache,
            self.platform.os_namespace,
            self.platform.board_namespace)
        self.assertEqual(
            self.platform.build_cache,
            self.stub_os.path.join(cache_root, self.platform.build_type))
        self.assertEqual(self.platform.sysroot,
                         self.stub_os.path.join(cache_root, 'sysroot'))
        self.assertEqual(self.platform.toolchain,
                         self.stub_os.path.join(cache_root, 'toolchain'))
        self.assertEqual(
            self.platform.product_out_cache,
            self.stub_os.path.join(
                self.platform.build_cache, 'target', 'product',
                self.platform.device.name))

    def test_init_bad_build_type(self):
        with self.assertRaises(platform.BuildTypeError):
            platform.Platform(os_name=self._OS,
                              os_version=self._OS_VERSION,
                              board=self._BSP,
                              board_version=self._BSP_VERSION,
                              build_type='not_a_build_type')

    def test_init_bad_board_version(self):
        with self.assertRaises(platform.BoardError):
            platform.Platform(os_name=self._OS,
                              os_version=self._OS_VERSION,
                              board=self._BSP,
                              board_version=self._BAD_VERSION,
                              build_type=self._BUILD_TYPE)

    def test_init_bad_board(self):
        with self.assertRaises(platform.BoardError):
            platform.Platform(os_name=self._OS,
                              os_version=self._OS_VERSION,
                              board='not_a_board',
                              board_version=self._BSP_VERSION,
                              build_type=self._BUILD_TYPE)

    def test_init_bad_os_version(self):
        with self.assertRaises(platform.OsError):
            platform.Platform(os_name=self._OS,
                              os_version=self._BAD_VERSION,
                              board=self._BSP,
                              board_version=self._BSP_VERSION,
                              build_type=self._BUILD_TYPE)

    def test_init_bad_os(self):
        with self.assertRaises(platform.OsError):
            platform.Platform(os_name='not_brillo',
                              os_version=self._OS_VERSION,
                              board=self._BSP,
                              board_version=self._BSP_VERSION,
                              build_type=self._BUILD_TYPE)

    def test_cache_path(self):
        cache_root = self.stub_os.path.join(
            self.stub_user_config.USER_CONFIG.platform_cache,
            self.platform.os_namespace,
            self.platform.board_namespace)
        self.assertEqual(
            self.platform.cache_path('a', 'nother', 'path'),
            self.stub_os.path.join(cache_root, 'a', 'nother', 'path'))

    def test_link(self):
        self.assertFalse(self.dev.is_linked)
        with self.platform.linked():
            self.assertTrue(self.dev.is_linked)
        self.assertFalse(self.dev.is_linked)

    def test_verify_downloaded(self):
        self.platform.verify_downloaded()

    def test_verify_board_not_downloaded(self):
        self.dev.downloaded = False
        with self.assertRaises(platform.NotDownloadedError):
            self.platform.verify_downloaded()

    def test_verify_os_not_downloaded(self):
        self.platform.os.available = False
        with self.assertRaises(platform.NotDownloadedError):
            self.platform.verify_downloaded()
