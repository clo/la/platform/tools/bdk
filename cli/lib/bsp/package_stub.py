#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""A stub for the Package class"""


from bsp import package
from bsp import status


class StubPackage(object):
    """Replacement for Packages"""
    from_dict_error = None

    def __init__(self, name='', remote='', version='', subpackages=None,
                 downloaded=True, licenses=None):
        self.name = name
        self.remote = remote
        self.version = version
        # Map of statuses, doubles as iterable of subpackages.
        self.subpackages = subpackages or {}
        self.should_remove = []
        # Map of {subpackages: post-link status}
        self.should_link = {}
        self.should_unlink = {}
        self.should_download = False
        self.should_uninstall = False
        self.downloaded = downloaded
        self.should_refresh = False
        self.licenses = licenses or []

    def is_downloaded(self):
        return self.downloaded

    def get_licenses(self, _subpackages):
        result = []
        for l in self.licenses:
            result.append(package.License(l, l))
        return result

    def subpackage_status(self, name, path=''):
        # Will error if no such subpackage
        stat = self.subpackages[name]
        if not self.downloaded:
            stat = status.NOT_INSTALLED
        if not path and stat <= status.UNRECOGNIZED:
            # Technically UNRECOGNIZED could still be MISSING too, but
            # this is a close enough approximation for current tests.
            stat = status.INSTALLED
        return (stat, '{} - {}'.format(name, status.status_string(stat)))

    # pylint: disable=unused-argument
    def download(self, tarball):
        if not self.downloaded and not self.should_download:
            raise package.DownloadError('Not supposed to download {}'.format(
                self.name))
        self.downloaded = True
        for s in self.subpackages:
            self.subpackages[s] = status.INSTALLED

    def uninstall(self):
        if not self.should_uninstall:
            raise OSError('Not supposed to uninstall {}'.format(self.name))
        self.downloaded = False
        for s in self.subpackages:
            self.subpackages[s] = status.NOT_INSTALLED

    def link_subpackage(self, name, path):
        if not self.downloaded:
            raise package.NotDownloadedError
        if not name in self.subpackages:
            raise package.NoSuchSubpackageError
        if not self.should_link.get(name) == path:
            raise package.SubpackageLinkError(
                'Only supposed to link {} to {} (requested {})'.format(
                    name, self.should_link.get(name), path))
        self.subpackages[name] = status.LINKED

    def unlink_subpackage(self, name, path):
        if not name in self.subpackages:
            raise package.NoSuchSubpackageError
        if not self.subpackages.get(name) == status.LINKED:
            raise package.SubpackageUnlinkError('{} is not linked'.format(name))
        if not self.should_unlink.get(name) == path:
            raise package.SubpackageUnlinkError(
                'Only supposed to unlink {} from {} (requested {})'.format(
                    name, self.should_unlink.get(name), path))
        self.subpackages[name] = status.INSTALLED

    @classmethod
    def from_dict(cls, _dic, name):
        if cls.from_dict_error:
            raise ValueError('Error.')
        else:
            return cls(name)

class StubTarPackage(object):
    """Replacement for the TarPackage class methods."""

    def __init__(self, tarball_version=''):
        self.tarball_version = tarball_version

    def get_tarball_version(self, _tarball):
        return self.tarball_version

class StubPackageModule(object):
    """Stubs for the package module as needed."""

    def __init__(self, tar_package=None):
        self.Error = package.Error
        self.NotDownloadedError = package.NotDownloadedError
        self.TarPackage = tar_package or StubTarPackage()
