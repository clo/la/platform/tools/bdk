#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""Unit tests for operating_system.py."""


import unittest

from bsp import operating_system
from core import user_config_stub
from core import util_stub
from test import stubs


class OperatingSystemTest(unittest.TestCase):

    _OS = 'os_name'
    _OS_VERSION = '99.11.00'
    _OS_ROOT = '/stuff/path'
    # Should not match any other version used above.
    _BAD_VERSION = '0.0'

    def setUp(self):
        self.stub_os = stubs.StubOs()
        self.stub_open = stubs.StubOpen(self.stub_os)
        self.stub_user_config = user_config_stub.StubUserConfig(
            os_root=self._OS_ROOT)
        self.stub_util = util_stub.StubUtil()

        operating_system.os = self.stub_os
        operating_system.open = self.stub_open.open
        operating_system.user_config = self.stub_user_config
        operating_system.util = self.stub_util

        self.version_file_path = self.stub_os.path.join(
            self._OS_ROOT, 'tools', 'bdk', 'VERSION')
        self.stub_os.path.should_exist = [self.version_file_path]
        self.stub_open.files[self.version_file_path] = stubs.StubFile(
            self._OS_VERSION)
        self.os = operating_system.OperatingSystem(name=self._OS,
                                                   root=self._OS_ROOT)

    def test_init_valid(self):
        # Check that values initialized correctly.
        self.assertEqual(self.os.name, self._OS)
        self.assertEqual(self.os.root, self._OS_ROOT)
        self.assertEqual(self.os.version, self._OS_VERSION)

    def test_init_missing_version(self):
        self.stub_os.path.should_exist = []
        with self.assertRaises(operating_system.VersionError):
            operating_system.OperatingSystem(name=self._OS, root=self._OS_ROOT)

    def test_init_bad_version(self):
        self.stub_open.files[self.version_file_path].contents = 'not_a_version'
        with self.assertRaises(operating_system.VersionError):
            operating_system.OperatingSystem(name=self._OS, root=self._OS_ROOT)

    def test_path(self):
        self.assertEqual(self.os.path(), self._OS_ROOT)
        self.assertEqual(self.os.path('sub', 'path'),
                         self.stub_os.path.join(self._OS_ROOT, 'sub', 'path'))

    def test_is_available(self):
        self.stub_os.path.should_be_dir = []
        self.assertFalse(self.os.is_available())
        self.stub_os.path.should_be_dir = [self._OS_ROOT]
        self.assertTrue(self.os.is_available())

    def test_default_os(self):
        os = operating_system.init_default_operating_system()
        self.assertEqual(os.name, 'brillo')
        self.assertEqual(os.root, self._OS_ROOT)
        self.assertEqual(os.version, self._OS_VERSION)

    def test_default_from_config(self):
        """Confirm the default OS is using the value from user_config."""
        self.stub_user_config.USER_CONFIG.os_root = 'root_is_elsewhere'
        # Should complain about missing version file.
        with self.assertRaisesRegexp(operating_system.VersionError,
                                     'root_is_elsewhere/tools/bdk/VERSION'):
            operating_system.init_default_operating_system()
