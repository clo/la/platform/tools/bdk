#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""Provides a class for BSP manifests."""


import json

from bsp import device
from bsp import package
from bsp import operating_system
from core import util
import error


DEFAULT_MANIFEST_FILE = util.GetBDKPath('cli', 'config', 'bsp_manifest.json')
MANIFEST_KEY_PACKAGES = 'packages'
MANIFEST_KEY_DEVICES = 'devices'


class Error(error.Error):
    """General failure."""


class UnknownBspError(Error):
    """Raised when a requested BSP is not recognized."""
    description = 'Unrecognized BSP name'


class Manifest(object):
    """Class for Manifests.

    Manifests are just dumb data.

    Attributes:
        packages: a dictionary of packages ({package_name : Package}).
        devices: a dictionary of devices ({short_name : Device}).
        operating_systems: a dictionary of OSes ({name : OperatingSystem}).
    """

    def __init__(self, packages, devices):
        self.packages = packages
        self.devices = devices
        default_os = operating_system.init_default_operating_system()
        self.operating_systems = {default_os.name: default_os}

    @classmethod
    def from_dict(cls, dic):
        """Create a Manifest from a dict.

        Args:
            dic: the dictionary to build the Manifest from.

        Returns:
            A Manifest created from |dic|.
        """
        packages = {}
        devices = {}
        for (package_name, package_spec) in (
                dic[MANIFEST_KEY_PACKAGES].iteritems()):
            try:
                packages[package_name] = package.Package.from_dict(package_spec,
                                                                   package_name)
            except KeyError as e:
                raise KeyError('package {}: {}'.format(package_name, e))

        for (short_name, device_spec) in dic[MANIFEST_KEY_DEVICES].iteritems():
            try:
                devices[short_name] = device.Device.from_dict(device_spec,
                                                              short_name,
                                                              packages)
            except KeyError as e:
                raise KeyError('device {}: {}'.format(short_name, e))

        return cls(packages, devices)

    @classmethod
    def from_json(cls, manifest_file=DEFAULT_MANIFEST_FILE):
        """Reads in a manifest from a json file.

        Args:
            manifest_file: the path to the file containing the manifest json.

        Returns:
            A manifest.Manifest object based on the file passed in.

        Raises:
            IOError: if there are issues opening the file.
            ValueError: if the specified file does not parse as valid json,
                or is not a valid manifest for reasons other than KeyErrors.
            KeyError: if a required manifest key is missing.
        """

        manifest_dic = {}
        try:
            with open(manifest_file) as f:
                manifest_dic = json.load(f)
        except IOError as e:
            raise IOError('Unable to open bsp manifest file {}: {}.'.format(
                manifest_file, e))
        except ValueError as e:
            raise ValueError('Could not parse json in '
                             'bsp manifest file {}: {}'.format(manifest_file,
                                                               e))

        try:
            result = cls.from_dict(manifest_dic)
        except KeyError as e:
            raise KeyError('Missing value in bsp manifest file {}: {}.'.format(
                manifest_file, e))
        except ValueError as e:
            raise ValueError('Error in bsp manifest file {}: {}.'.format(
                manifest_file, e))

        return result

    def is_bsp_available(self, bsp):
        """Checks that the requested BSP is available for building.

        Args:
            bsp: BSP name.

        Returns:
            True if the BSP is available, False otherwise.

        Raises:
            UnknownBspError: The given BSP name is not a known BSP.
        """
        if bsp not in self.devices:
            raise UnknownBspError(bsp)

        return self.devices[bsp].is_available()
