#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Provides a class for BSP packages."""


from collections import namedtuple
import hashlib
import os
import shutil

from bsp import status
from bsp import subpackage
from core import tool
from core import user_config
import error


PACKAGE_KEY_TYPE = 'package_type'
PACKAGE_KEY_REMOTE = 'remote'
PACKAGE_KEY_VERSION = 'version'
PACKAGE_KEY_SUBPACKAGES = 'subpackages'

GIT_PACKAGE_TYPE = 'git'
TAR_PACKAGE_TYPE = 'tar'

BSP_PACKAGE_DIR_FORMAT = os.path.join(
    '{name}', 'source_versions', '{source_version}')
DEFAULT_TAR_FILE = 'tarball.tar.gz'


class Error(error.Error):
    description = 'Error in Package'


class DownloadError(Error):
    """Raised when a Package fails to download."""
    description = 'Failed to download package'


class RemoteError(DownloadError):
    """Raised when there is a problem checking the remote version of a
    Package.
    """
    description = 'Trouble getting remote version'


class VersionError(DownloadError):
    """Raised when the package source is not/doesn't have the correct
    version.
    """
    description = 'Failed to get correct version'


class UnzipError(DownloadError):
    """Raised when the downloaded tarball fails to unzip correctly."""
    description = 'Failed to unzip tarball'


class MissingDirectoryError(Error):
    """Raised when an expected directory is missing."""
    description = 'Directory is missing'


class NotDownloadedError(Error):
    """Raised when a package isn't downloaded but needs to be."""
    description = 'Package is not downloaded'


class NoSuchSubpackageError(Error):
    """Raised when a non-existant subpackage is requested."""
    description = 'No such subpackage'


class PathBlockedError(Error):
    """Raised when a path name is taken."""
    description = 'Unknown file/dir in the way. Must be manually removed'


class SubpackageLinkError(Error):
    """Raised when there is trouble linking a subpackage."""
    description = 'Trouble linking subpackage'


class SubpackageUnlinkError(Error):
    """Raised when there is trouble unlinking a subpackage."""
    description = 'Trouble unlinking subpackage'


License = namedtuple('License', ('name', 'path'))


class Package(object):
    """Class for packages.

    Attributes:
        name: the name of the package.
        remote: the remote location of the package.
        version: the version of the package.
        subpackages: A dictionary of subpackages
            ({ subpackage_name : Subpackage }).
        directory: (read-only) the local directory for the package.
    """

    def __init__(self, name, remote, version, subpackages):
        self.name = name
        self.remote = remote
        self.version = version
        self.subpackages = subpackages

    @property
    def directory(self):
        return os.path.abspath(os.path.join(
            user_config.USER_CONFIG.bsp_dir, BSP_PACKAGE_DIR_FORMAT.format(
                name=self.name, source_version=self.version)))

    def _get_subpackage(self, name):
        """Gets a subpackage of the given name.

        Args:
            name: The name of the subpackage to get.

        Returns:
            The desired subpackage.

        Raises:
            NoSuchSubpackageError: If no subpackage with the desired name
            exists.
        """
        subpackage_ = self.subpackages.get(name)
        if not subpackage_:
            raise NoSuchSubpackageError(name)
        return subpackage_

    def is_downloaded(self):
        return os.path.isdir(self.directory)

    def subpackage_status(self, name, path=''):
        """Checks the status of a given subpackage.

        Args:
            name: the name of the subpackage to check status of. ValueError if
                 self has no subpackage matching the given name.
            path: (optional) The path where the subpackage is linked from.
                If empty, do not check for UNRECOGNIZED/LINKED.

        Returns:
            (status, status_string) pair, where status is, in order:
            status.UNRECOGNIZED: if the given path exists.
            status.NOT_INSTALLED: if the package isn't downloaded.
            status.MISSING: if the subpackage subdir isn't a dir.
            status.LINKED: if the subpackage subdir is present and
                linked to from the given path.
            status.INSTALLED: if the package is downloaded and the subdir
                present.
        """
        subpackage_ = self._get_subpackage(name)
        subdir = os.path.join(self.directory, subpackage_.subdir)
        subdir_is_dir = os.path.isdir(subdir)
        if os.path.exists(path):  # '' does not exist.
            if (os.path.islink(path) and
                    subdir_is_dir and
                    os.path.samefile(path, subdir)):
                result = status.LINKED
            else:
                result = status.UNRECOGNIZED
        elif os.path.islink(path):
            # Is link but doesn't exist = broken link.
            # For now, this is also "unrecognized".
            result = status.UNRECOGNIZED
        elif not self.is_downloaded():
            result = status.NOT_INSTALLED
        elif not os.path.isdir(subdir):
            result = status.MISSING
        else:
            # It is downloaded and exists.
            result = status.INSTALLED

        return (result, '{} - {}'.format(name, status.status_string(result)))

    def download(self, tarball=None):
        """Downloads the package files (or just extracts them from a tarball).

        Only downloads if the package is not already downloaded.

        Args:
            tarball: (optional) if provided, replaces the download part of tar
                download and skips straight to unzipping.

        Raises:
            DownloadError: (or a child thereof) if the package fails to complete
                all steps of the download process (downloading, verifying
                version, etc.).
            PathBlockedError: if there is an unknown file/dir at the
                target download location.
            ValueError: if a tarball is passed to a git package.
        """
        if self.is_downloaded():
            return

        if os.path.exists(self.directory):
            raise PathBlockedError('Intended download location {}.'.format(
                self.directory))

        success = False
        try:
            os.makedirs(self.directory)
            self._type_specific_download(tarball)
            success = True
        finally:
            if not success:
                self.uninstall()

    def uninstall(self):
        """Uninstalls a package, removing the download.

        Note that this may result in dead links in tree if the package was
        linked.

        Raises:
            OSError: if there is an unexpected problem uninstalling.
        """
        if self.is_downloaded():
            shutil.rmtree(self.directory)

    def _type_specific_download(self, tarball=None):
        """Do package type specific downloading.

        Args:
            tarball: (optional) if provided, replaces the download part of tar
                download and skips straight to unzipping.

        Raises:
            ValueError: If a tarball is passed to a git package.
            DownloadError: (or a subclass thereof) if there is some complication
                downloading.
        """
        raise NotImplementedError(
            'Download function should be overridden in child class.')

    def get_licenses(self, subpackage_names):
        """Get all licenses for all subpackages.

        Args:
            subpackage_names: a list of names of subpackages to get licenses of.

        Returns:
            A list of License namedtuples.

        Raises:
            NoSuchSubpackageError: If any of the subpackages are missing.
        """
        result = []
        subpackages = [self._get_subpackage(name)
                       for name
                       in subpackage_names]
        for s in subpackages:
            result += [License('{}.{} - {}'.format(self.name, s.name, path),
                               os.path.join(self.directory, s.subdir, path))
                       for path
                       in s.licenses]

        return result

    def link_subpackage(self, name, path):
        """Links a subpackage into the BDK.

        Args:
            name: the name of the subpackage to link. ValueError if
                self has no subpackage matching the given name.
            path: the path where the subpackage should be linked from.

        Raises:
            SubpackageLinkError: if the subpackage fails to link.
            MissingDirectoryError: if the subpackage's subdir is missing.
            NoSuchSubpackageError: if name isn't a valid subpackage name.
            NotDownloadedError: if the package isn't downloaded.
        """
        if not self.is_downloaded():
            raise NotDownloadedError()
        subpackage_ = self._get_subpackage(name)
        subdir = os.path.join(self.directory, subpackage_.subdir)
        if not os.path.isdir(subdir):
            raise MissingDirectoryError('{} for subpackage {}'.format(
                subpackage_.subdir, subpackage_.name))

        try:
            parent_dir = os.path.dirname(path)
            if parent_dir and not os.path.isdir(parent_dir):
                os.makedirs(parent_dir)
            os.symlink(subdir, path)
        except OSError as e:
            raise SubpackageLinkError(
                '{} subdir {}: {}'.format(
                    subpackage_.name, subpackage_.subdir, e))

    def unlink_subpackage(self, name, path):
        """Unlinks a given subpackage.

        The subpackage must be linked to start with.

        Args:
            name: the name of the subpackage to unlink.
            path: the path where the subpackage is linked from.

        Raises:
            NoSuchSubpackageError: if self has no subpackage matching the given
                name.
            SubpackageUnlinkError: if there is trouble unlinking.
        """
        (status_, _) = self.subpackage_status(name, path)
        if status_ == status.LINKED:
            try:
                os.remove(path)
            except OSError as e:
                raise SubpackageUnlinkError(
                    '{} (linked from {}): {}.'.format(
                        name, path, e))
        else:
            raise SubpackageUnlinkError('{} is not linked from {}.'.format(
                name, path))

    @classmethod
    def from_dict(cls, dic, name):
        """Create a Package from a dict.

        Args:
            dic: the dictionary to build the package from.
            name: the name of the package.

        Returns:
            A Package with the given name generated from |dic|.

        Raises:
            KeyError: If a required key is missing.
            ValueError: If an invalid value is in dic.
        """
        package_type = dic[PACKAGE_KEY_TYPE]
        package_subclass = None
        if package_type == GIT_PACKAGE_TYPE:
            package_subclass = GitPackage
        elif package_type == TAR_PACKAGE_TYPE:
            package_subclass = TarPackage
        else:
            raise ValueError('Package type must be either {} or {}.'.format(
                GIT_PACKAGE_TYPE, TAR_PACKAGE_TYPE))

        subpackages = {}
        for (subpackage_name, data) in dic[PACKAGE_KEY_SUBPACKAGES].iteritems():
            try:
                subpackages[subpackage_name] = subpackage.Subpackage.from_dict(
                    data, subpackage_name)
            except KeyError as e:
                raise KeyError('subpackage {}: {}'.format(name, e))

        return package_subclass(name,
                                dic[PACKAGE_KEY_REMOTE],
                                dic[PACKAGE_KEY_VERSION],
                                subpackages)


class GitPackage(Package):

    def _type_specific_download(self, tarball=None):
        """Download a Git repo. See Package.Download."""
        if tarball:
            raise ValueError('Initializing Git packages from a tarball '
                             'is not currently supported.')
        # Setup. Break down version into branch + hash
        # Use rsplit for the off chance ':' is part of the branch name.
        (branch, branch_hash) = self.version.rsplit(':', 1)
        git = tool.PathToolWrapper('git')

        # Check if branch points to hash
        # If so, we can do a shallow clone
        try:
            out, _ = git.run(['ls-remote', self.remote, branch], piped=True)
        except tool.Error as e:
            raise RemoteError(e)
        if out.startswith(branch_hash):
            shallow = True
            depth = '--depth=1'
        else:
            shallow = False
            depth = '--single-branch'

        # Clone the repo.
        try:
            git.run(['clone', depth, '--branch=' + branch,
                     self.remote, self.directory])
        except tool.Error as e:
            raise DownloadError('from {}: {}'.format(self.remote, e))

        # Checkout the right revision if necessary
        if not shallow:
            try:
                git.set_cwd(self.directory)
                git.run(['reset', '--hard', branch_hash])
                git.set_cwd(None)
            except tool.Error as e:
                raise VersionError(e)

        # Check that the correct revision was downloaded.
        git_dir = os.path.join(self.directory, '.git')
        try:
            out, _ = git.run(['--git-dir=' + git_dir, 'rev-parse', branch],
                             piped=True)
        except tool.Error as e:
            raise VersionError(e)

        if not out.startswith(branch_hash):
            raise VersionError('Remote package from {} '
                               'is the incorrect version '
                               '{} (expected {}).'.format(
                                   self.remote, out, branch_hash))

        # Everything went well. Clean up the download; we don't need git
        # history.
        shutil.rmtree(git_dir)


class TarPackage(Package):

    @classmethod
    def get_tarball_version(cls, tarball_file):
        """Gets the tarball version (sha256) of a tarball file."""
        try:
            with open(tarball_file) as tar:
                tar_hash = hashlib.sha256(tar.read()).hexdigest()
        except IOError as e:
            raise VersionError(e)
        return tar_hash

    def _type_specific_download(self, tarball_file=None):
        """Download a tarball package. See Package.Download."""
        tarball_provided = (tarball_file is not None)
        # Store the source for error messages.
        tar_source = tarball_file if tarball_provided else self.remote

        tool_runner = tool.PathToolRunner()

        # Get the tarball if it wasn't provided
        if not tarball_provided:
            # Setup.
            tarball_file = os.path.join(self.directory, DEFAULT_TAR_FILE)

            # Download the tarball.
            try:
                tool_runner.run('curl', ['-o', tarball_file, self.remote])
            except tool.Error as e:
                raise DownloadError(e)

        try:
            # Check version of the tarball by using a checksum
            tar_hash = self.get_tarball_version(tarball_file)
            if not tar_hash.startswith(self.version):
                raise VersionError('Tarball from {} is the incorrect version '
                                   '{} (expected {})'.format(
                                       tar_source, tar_hash, self.version))

            # Unzip the tarball
            try:
                tool_runner.run('tar', ['-C', self.directory, '-xzf',
                                        tarball_file])
            except tool.Error as e:
                raise UnzipError(e)

        finally:
            # Whether extraction succeeds or fails,
            # we should remove the downloaded tarball.
            if not tarball_provided:
                os.remove(tarball_file)
