#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""Stubs for manifest class as needed."""


import error

from bsp import operating_system_stub


class StubManifest(object):

    from_json_devices = {}
    operating_system = operating_system_stub.StubOperatingSystemModule()

    def __init__(self, devices):
        """devices should be {name: device}."""
        self.devices = devices
        default_os = self.operating_system.init_default_operating_system()
        self.operating_systems = {default_os.name: default_os}

    def is_bsp_available(self, device):
        if not device in self.devices:
            raise StubManifestModule.UnknownBspError(
                'No such device {}, options are {}'.format(
                    device, self.devices.keys()))
        return self.devices[device].is_available()

    # pylint: disable=unused-argument
    @classmethod
    def from_json(cls, manifest_file=None, os_path=None):
        return cls(cls.from_json_devices)


class StubManifestModule(object):

    class Error(error.Error):
        pass

    class UnknownBspError(Error):
        pass

    def __init__(self, operating_system_module):
        self.Manifest = StubManifest
        self.Manifest.operating_system = operating_system_module

    def Manifest(self, *args, **kwargs):
        return StubManifest(*args, **kwargs)
