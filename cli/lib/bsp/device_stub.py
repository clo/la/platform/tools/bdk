#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""A stub for the Device class."""


from bsp import device


class Error(device.Error):
    """Raised when a caller attempts to link to the incorrect OS version."""


class StubDevice(object):

    def __init__(self, name='', full_name='', vendor='', arch='',
                 package_map=None, version='', downloaded=False,
                 is_linked=False):
        self.name = name
        self.full_name = full_name
        self.vendor = vendor
        self.arch = arch
        self.version = version
        self.package_map = package_map or {}
        self.is_linked = is_linked
        self.downloaded = downloaded

    def is_available(self):
        return self.downloaded

    def link(self, _os):
        self.is_linked = True

    def unlink(self, _os):
        self.is_linked = False
