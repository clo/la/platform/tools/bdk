#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Provides a class for BSP devices.

Also includes a hook function to decode dictionaries into
Device objects.
"""

import os

from bsp import package
from bsp import status
from core import util
import error


DEVICE_KEY_FULL_NAME = 'device_name'
DEVICE_KEY_PACKAGES = 'packages'
DEVICE_KEY_VENDOR = 'vendor'
DEVICE_KEY_ARCH = 'arch'


class Error(error.Error):
    description = 'Error with Device'


class LicenseError(Error):
    """Raised when there is an error with licensing."""
    description = 'Error approving licenses'


class PackageDownloadError(Error):
    """Raised when a package for the device has trouble downloading."""
    description = 'Error downloading package'


class PackageLinkError(Error):
    """Raised when a subpackage for the device fails to link properly."""
    description = 'Failed to link BSP package to OS tree'


class PackageUnlinkError(Error):
    """Raised when a subpackage for the device fails to unlink properly."""
    description = 'Failed to unlink BSP from OS tree'


class Device(object):
    """Class to represent devices with BSPs available.

    Attributes:
        name: the short name of the package.
        full_name: the full name of the package.
        vendor: the vendor of the device.
        arch: the architecture of the device.
        version: the version of the device.
        _package_map: a mapping of {package.Package: {subpackage_name: path}},
            informing which subpackages of which packages this device requires,
            and where in the OS it expects them to be installed
            (path is relative to OS root).
    """

    def __init__(self, name, full_name, vendor, arch, package_map):
        """Initializes a Device.

        Args:
            name: the name of the device.
            vendor: the vendor of the device.
            arch: the architecture of the device.
            package_map: a mapping of
                {package.Package: {subpackage_name: relative_path}},
                where relative_path is relative to the BDK root.
        """
        self.name = name
        self.full_name = full_name
        self.vendor = vendor
        self.arch = arch
        self.version = '0.0.0'
        self._package_map = package_map

    def status(self, os_=None, verbose=False):
        """Checks the status of a device.

        Args:
            os_: (optional) The os to check the status of this device against.
                If empty, ignore LINKED/UNRECOGNIZED; only worry about
                INSTALLED/MISSING/NOT_INSTALLED.
            verbose: (optional) If True, also return details of subpackages.

        Returns:
            (status, status string), where where status is the highest status
            of any subpackage that is part of the device BSP, or
            status.INSTALLED if the BSP has no subpackages. The status string
            lists the device and human-readable status, and in verbose mode
            also includes the same for each subpackage.
        """
        device_status = status.INSTALLED
        packages_string = ''
        for (pkg, subpackage_map) in self._package_map.iteritems():
            packages_string += '\n * {0}'.format(pkg.name)
            for (subpackage, relpath) in subpackage_map.iteritems():
                subpackage_path = ''
                if os_:
                    subpackage_path = os_.path(relpath)
                (subpackage_status, subpackage_string) = pkg.subpackage_status(
                    subpackage, subpackage_path)
                packages_string += '\n    * {0}'.format(subpackage_string)
                device_status = max(device_status, subpackage_status)

        device_string = '{} {} - {}'.format(self.full_name, self.version,
                                            status.status_string(device_status))
        if verbose:
            device_string += packages_string

        return (device_status, device_string)

    def is_available(self):
        """Checks whether a device is available for use.

        Returns:
            True if the BSP status is status.INSTALLED,
            False otherwise.
        """
        return self.status()[0] == status.INSTALLED

    def link(self, os_):
        """Links an OS to this device.

        Does not overwrite anything; ignores situations where it would need to.

        Args:
            os_: The OS to link to this device.

        Raises:
            PackageLinkError: if a package problem occurs while linking.
            util.OSVersionError: if the OS version is invalid.
        """
        errors = []
        for (pkg, subpackage_map) in self._package_map.iteritems():
            for (subpackage_name, relpath) in subpackage_map.iteritems():
                link = os_.path(relpath)
                # Don't overwrite anything.
                if os.path.exists(link):
                    continue

                try:
                    pkg.link_subpackage(subpackage_name, link)
                except package.Error as e:
                    errors.append('package {}: {}'.format(pkg.name, e))
                    # Error with the whole package, not just a single
                    # subpackage, don't bother with the rest of the package.
                    if isinstance(e, package.NotDownloadedError):
                        break

        if errors:
            raise PackageLinkError('{} to {}: {}'.format(self.name,
                                                         os_, errors))

    def unlink(self, os_):
        """Unlinks this device from an OS.

        Removes links of LINKED subpackages.

        Args:
            os_: The OS to unlink from.

        Raises:
            PackageUnlinkError: if a package problem occurs while unlinking.
            util.OSVersionError: if the OS version is invalid.
        """
        errors = []
        for (pkg, subpackage_map) in self._package_map.iteritems():
            for (subpackage, relpath) in subpackage_map.iteritems():
                link = os_.path(relpath)
                try:
                    subpackage_status, _ = pkg.subpackage_status(
                        subpackage, link)
                    if subpackage_status == status.LINKED:
                        pkg.unlink_subpackage(subpackage, link)
                except package.Error as e:
                    errors.append('{}: {}'.format(pkg, e))
        if errors:
            raise PackageUnlinkError('{} from {}: {}'.format(self.name, os_,
                                                             errors))

    def unrecognized_paths(self, os_):
        """Get the paths of unrecognized subpackages of this device.

        Args:
            os_: The OS to check for unrecognized paths in.

        Returns:
            A list of paths of unrecognized subpackages of this device.
        """
        result = []
        for (pkg, subpackage_map) in self._package_map.iteritems():
            for (subpackage, relpath) in subpackage_map.iteritems():
                subpackage_path = os_.path(relpath)
                status_, _ = pkg.subpackage_status(subpackage, subpackage_path)
                if status_ == status.UNRECOGNIZED:
                    result.append(subpackage_path)
        return result

    def match_tarball(self, tarball):
        """Matches a tarball to its corresponding package.

        Args:
            tarball: a path to a tarball file.

        Returns:
            The name of the matching package, if any.
            None otherwise.

        Raises:
            package.VersionError, if there is a problem reading the version.
        """
        tar_hash = package.TarPackage.get_tarball_version(tarball)
        for pkg in self._package_map:
            if tar_hash.startswith(pkg.version):
                return pkg.name

        return None

    def _prompt_licenses(self, auto_accept=False):
        """Helper - finds and prompts all license files interactively.

        Args:
            auto_accept: (optional) If true, still prints a list of licenses,
                but automatically accepts them.

        Returns:
            True if licenses were accepted, False otherwise.

        Raises:
            LicenseError: if there is a problem reading a license.
            package.NoSuchSubpackageError: if the package map is not correctly
                validated and specifies a non-existant subpackage.
        """
        # Find all the licenses.
        licenses = []
        for (pkg, subpackage_map) in self._package_map.iteritems():
            licenses += pkg.get_licenses(subpackage_map.keys())

        # If there aren't any, great!
        num_licenses = len(licenses)
        if num_licenses == 0:
            return True

        # Write a prompt.
        prompt = (
            'This package contains software provided by a third party '
            'with different license terms. Please read and make sure you '
            'understand each of the following licenses before choosing '
            'accept.\n'
        )
        for i in range(num_licenses):
            prompt += '{}. {}\n'.format(i+1, licenses[i].name)
        if num_licenses == 1:
            prompt += '(1) View License\n'
        else:
            prompt += '(1-{}) View License\n'.format(num_licenses)
        prompt += ('(A) Accept All\n'
                   '(C) Cancel')

        # Ask the user to accept.
        confirmed = auto_accept or None
        while confirmed is None:
            choice = util.GetUserInput(prompt).strip().upper()
            if choice.isdigit():
                choice = int(choice)
                if choice > 0 and choice <= num_licenses:
                    # View license.
                    try:
                        with open(licenses[choice - 1].path) as f:
                            print f.read()
                    except IOError as e:
                        raise LicenseError(
                            'failed to read license {} ({}): {}'.format(
                                choice, licenses[choice - 1].name, e))
                else:
                    # Invalid number.
                    if num_licenses == 1:
                        print 'The only license available to view is 1.'
                    else:
                        print ('Please specify a license number from '
                               '1-{}.'.format(num_licenses))
            elif choice == 'A':
                confirmed = True
            elif choice == 'C':
                confirmed = False
            else:
                print ('Unrecognized option "{}". Please specify "A", "C", '
                       'or a license number to view.'.format(choice))

        return confirmed

    def install(self, extract_only=None, auto_accept=False, link_os=None,
                verbose=False):
        """Installs the BSP for this device.

        Args:
            extract_only: (optional) a map of { package_name : tarball_file },
                for packages to skip the tarball download step.
            auto_accept: (optional) True to accept all licenses automatically.
            link_os: (optional) An OS to link packages into.
                Intended only for when developing the OS/BSP, not for use with
                product development flow.
            verbose: (optional) If True, print status messages. Default False.

        Returns:
            True if the installation is successful, False otherwise.

        Raises:
            LicenseError: if the user does not accept all licenses.
            PackageLinkError: if a package fails to link to the tree.
                (Will not occur if link_os is None)
            PackageDownloadError: if a required bsp.Package fails to download.
            package.NoSuchSubpackageError: if the package map is not correctly
                validated and specifies a non-existant subpackage.
        """
        extract_only = extract_only or {}
        downloaded = []
        licenses_confirmed = False

        try:
            # Download all missing packages, tracking which these are.
            for pkg in self._package_map:
                if not pkg.is_downloaded():
                    if verbose:
                        print 'Downloading {}...'.format(pkg.name)
                    try:
                        pkg.download(tarball=extract_only.get(pkg.name))
                        downloaded.append(pkg)
                    except package.Error as e:
                        raise PackageDownloadError('{}: {}'.format(pkg.name, e))
                elif verbose:
                    print '{} already downloaded...'.format(pkg.name)

            # Find all licenses and give the interactive prompt.
            licenses_confirmed = self._prompt_licenses(auto_accept=auto_accept)
            # If licenses weren't confirmed, we're done.
            if not licenses_confirmed:
                raise LicenseError('user did not accept all licenses.')
        finally:
            # If anything failed, remove what we downloaded.
            if not licenses_confirmed:
                for pkg in downloaded:
                    pkg.uninstall()

        # If requested, go ahead and install the links.
        if link_os:
            self.link(link_os)

    def uninstall(self):
        """Uninstalls all BSP packages for a device.

        Raises:
            OSError: if there is an unexpected problem uninstalling.
        """
        for pkg in self._package_map:
            pkg.uninstall()

    @classmethod
    def from_dict(cls, dic, name, packages):
        """Create a Device from a dict.

        Merges together the name-based package identification with
        assembled Package objects.

        Args:
            dic: the dictionary to build the device from.
            name: the name of the device.
            packages: a dictionary mapping { package_name : Package }

        Returns:
            A Device generated from |dic| and |packages|.

        Raises:
            KeyError: An expected key is missing.
            ValueError: A non-existant package or subpackage name is used.
        """
        full_name = dic[DEVICE_KEY_FULL_NAME]
        package_map = {}
        for (package_name, subpackage_map) in (
                dic[DEVICE_KEY_PACKAGES].iteritems()):
            pkg = packages.get(package_name)
            if not pkg:
                raise ValueError(
                    'Package {} for {} does not exist.'.format(package_name,
                                                               full_name))
            for subpackage in subpackage_map:
                if subpackage not in pkg.subpackages:
                    raise ValueError(
                        'Subpackage {}.{} for {} does not exist.'.format(
                            package_name, subpackage, full_name))
            package_map[pkg] = subpackage_map

        return cls(name, full_name, dic[DEVICE_KEY_VENDOR],
                   dic[DEVICE_KEY_ARCH], package_map)
