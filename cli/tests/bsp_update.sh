#!/bin/bash

# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

. tests/test_helpers

setup_working_path

stub_git
stub_tar
stub_curl

echo "Testing bdk bsp update"
# Note: download, update, and install should all be aliases for the same thing.
# This tests updating a messed up bdk tree, bsp_download.sh tests
# install from nothing in the bdk tree, bsp_install.sh tests installing from an
# already present tarball.

# Start with a screwed up tree with a broken link.
TEST_DIR="${TEST_TREE}/bsp/git"
mkdir -p ${TEST_DIR}
touch target
ln -s target ${TEST_DIR}/git_subpackage
rm target
touch ${TEST_TREE}/bsp/tar2

common_args=("-m" "${DATA_DIR}/test_manifest.json")

${BDK} bsp status test_board "${common_args[@]}" |\
 grep "Test Board 0.0.0 - Not Installed"
${BDK} bsp update test_board --accept_licenses "${common_args[@]}"
# Update will not actually touch anything that already exists.
${BDK} bsp status test_board "${common_args[@]}" |\
 grep "Test Board 0.0.0 - Unrecognized"
# Definitely don't remove an unrecognized file.
${BDK} bsp status test_board "${common_args[@]}" |\
 grep "subpackage2 - Unrecognized"
# Even broken links should remain in place.
${BDK} bsp status test_board "${common_args[@]}" |\
 grep "subpackage - Unrecognized"
