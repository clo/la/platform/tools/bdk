#!/bin/bash

# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

. tests/test_helpers

setup_working_path

stub_git
stub_tar
stub_curl

common_args=("-m" "${DATA_DIR}/test_manifest.json")

echo "Testing that bsp packages are shared among devices"
${BDK} bsp download test_board -a "${common_args[@]}"
# Make sure no downloading happens for sharing board.
stub_dead_git
stub_dead_curl
${BDK} bsp download sharing_board -a "${common_args[@]}"

# Their statuses should be intertwined as well.
${BDK} bsp status test_board "${common_args[@]}" |\
 grep "Test Board 0.0.0 - Installed"
${BDK} bsp status sharing_board "${common_args[@]}" |\
 grep "Sharing Board 0.0.0 - Installed"
rm -r ${TEST_BSPs}/tar_package
${BDK} bsp status test_board "${common_args[@]}" |\
 grep "Test Board 0.0.0 - Not Installed"
${BDK} bsp status sharing_board "${common_args[@]}" |\
 grep "Sharing Board 0.0.0 - Not Installed"
