#!/bin/bash

# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

. tests/test_helpers

setup_working_path

stub_git
stub_tar
stub_curl

echo "Testing bdk bsp download with linkage."

${BDK} bsp download test_board -a -l -m "${DATA_DIR}/test_manifest.json"
# All checks are obviously dependent upon how test_bard is defined
# in test_manifest.

# Check that the links exist. For ease of writing this code we assume
# the source versions are correct.
readlink "${TEST_TREE}/bsp/git/git_subpackage" |\
 grep "${TEST_BSPs}/git_package/source_versions/.*/git_subdir"
readlink "${TEST_TREE}/bsp/tar1" |\
 grep "${TEST_BSPs}/tar_package/source_versions/.*/tar/sub1"
readlink "${TEST_TREE}/bsp/tar2" |\
 grep "${TEST_BSPs}/tar_package/source_versions/.*/tar/sub2"
