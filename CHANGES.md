Milestone 7
===========

Changes done in each Brillo Development Kit (BDK) release. Other places to get news about Brillo are TBD

You can discuss the latest BDK changes by subscribing to TBD

BDK 7.5.6 has been released on Thursday, January 28, 2016

**Summary**: This release contains the bare minimum for getting started. The Brillo Development Kit (BDK) current works with the Qualcomm Dragonboard and Intel Edison board for development and testing purposes. Basic weave commands are supported on these boards as well as use of the Weave Developer Console. Details follow.

1. Features
    1. Bdk command
    2. Support for fastboot and adb
    3. Support for custom kernel config
    4. Support for A/B updates via update_engine daemon
    5. Basic audio support and an Audio HAL
    6. Manual End to End tests via Autotest with a Moblab in a VM
    7. Open Sourced with Apache2 license
    8. Weave supported on devices
    9. Support for: updates, metrics, crash reports and Weave commands via the Weave
    10. Developer Console
2. Supported Boards
    1. Qualcomm Dragonboard
    2. Intel Edison
    3. ARM and x86 Emulators

Features
--------

### Bdk Commands

* `version` - Display BDK version
* `bsp`
    * `list` - List available BSPs, and their current installation status.
    * `download` - Download a BSP. The software that you are downloading is the property of the software owner and is distributed by the software owner. Google is providing the download service as a convenience.
    * `uninstall` - Uninstall BSP.
* `config`
    * `metrics` - Check or control what data you are sending to the Brillo team.
* `product`
    * `create` - Create a product project in the current directory
    * `envsetup` - Emit the environment for the product in the CWD
    * `gdb` - Run gdbclient.py for a given product
    * `tool` - Run a host tool for a given product
    * `build` - Build a product project from the current directory
    * `provision` - Run provision-device for a given product

A one-pager for this work can be found here <[link]>.

For details about the internal design of this feature, see this Design Doc
<[link]>.

Code:
<[link](https://android.googlesource.com/platform/tools/bdk/+log/brillo-m7-mr-dev)>

### Support for fastboot and adb
fastboot is the preferred method of communicating with a bootloader. Brillo will use fastboot for all devices. ADB is the standard Debug Bridge used for debugging Android Devices and now Brillo devices.

More documentation about fastboot can be found here
<[link](https://android.googlesource.com/platform/system/core/+/master/fastboot/fastboot_protocol.txt)>.

More documentation about adb can be found here
<[link](http://developer.android.com/tools/help/adb.html)>.

Developer Site for bootload:
<[link](https://developers.google.com/brillo/eap/guides/develop/bootloader)>

Developer Site for ADB:
<[link](https://developers.google.com/brillo/eap/guides/testing-debugging/debugging-tools)>

Code:
<[link](https://android.googlesource.com/platform/system/core/+/master/adb/)>

### Support for custom kernel config
In your product’s .mk file:

```makefile
include device/generic/brillo/brillo_base.mk
$(call add_kernel_configs, $(realpath $(LOCAL_PATH)/my_kernel.config))
```

Code:
<[link](https://android.googlesource.com/device/generic/brillo/+/brillo-m7-mr-dev/brillo_base.mk)>

### Push updates to a device via the Cloud (OTA)
The Brillo OTA (over-the-air) update feature makes it easy for product developers to securely, reliably update products in the field. It enables the following scenarios:

* Quickly address a serious bug or security problem.
* Simplify your marketing and support by having nearly all products on the latest software.
* Continue to add features to a product post-launch, keeping your users happy.

This page describes the entire update cycle for Brillo devices, from the developer creating an updated image to the product running updated software.

Developer Site Documentation:
<[link](https://developers.google.com/brillo/eap/guides/maintain/ota-updates)>

Codelab: <[link](https://www.code-labs.io/codelabs/brillo-ota/#0)>

Code:
<[link](https://android.googlesource.com/platform/system/update_engine/+log/brillo-m7-mr-dev)>

### Basic audio support and an Audio HAL
Audio in and out support via a hardware abstraction layer (HAL) for all supported boards. We’ve added the Audio HAL tests to the Platform Test Suite (PTS).

Code:
<[link](https://android.googlesource.com/device/generic/brillo/+/bb97d49da8db680b5fc8cc738f6f754bb7717bb7/pts/audio/)>

### Weave supported on devices
Weave is a communications platform for IoT devices that enables device setup, phone-to-device-to-cloud communication, and user interaction from mobile devices and the web.

Brillo’s support for Weave involves wrapping libWeave in a daemon (weaved)

Developer Site: <[link](https://developers.google.com/weave/)>

libWeave v1.0 Code:
<[link](https://weave.googlesource.com/weave/libweave/+log/weave-release-1.0)>

weaved Code:
<[link](https://android.googlesource.com/platform/system/weaved/+log/brillo-m7-mr-dev)>

### Weave Developer Console: Metrics, Crashes, OTA, Weave Commands
Developer Site - Metrics:
<[link](https://developers.google.com/brillo/eap/guides/maintain/monitoring)>

Developer Site - Crash Reporting:
<[link](https://developers.google.com/brillo/eap/guides/maintain/crash-reporting)>

### PTS
Platform Test Suite will be the means of certifying a board. While the current suite of tests is growing, there is still a lot of work happening on the test framework. Stay tuned for future releases.

Code:
<[link](https://android.googlesource.com/device/generic/brillo/+log/brillo-m7-mr-dev/pts)>

### Open Sourced with Apache2 license
Using the default Android Open Source Project (AOSP) licenses for Brillo, we’ve decided to move all development to the open. Only certain aspects of Weave and new board bring-up will be developed within Google.

Supported Boards
----------------
Use [this
page](https://developers.google.com/brillo/eap/guides/get-started/get-hardware) to see the current list of supported hardware.

### Qualcomm Dragonboard 410c
### Intel Edison
