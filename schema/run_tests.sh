#!/bin/bash
# Copyright 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

pass=0
pass_tests=0
ok=1
for x in testcases/pass/*.xml; do
  pass_tests=$((pass_tests + 1))
  test $ok -eq 0 && echo fail
  ok=0
  echo -n "Running $x . . . "
  (xmllint --noout $x --relaxng bdk.rng &>> test.log) || continue
  (jing bdk.rng $x &>>test.log) || continue
  echo ok
  ok=1
  pass=$((pass+1))
done

for x in testcases/fail/*.xml; do
  pass_tests=$((pass_tests + 1))
  test $ok -eq 0 && echo fail
  ok=0
  echo -n "Running $x . . . "
  (xmllint --noout $x --relaxng bdk.rng &>> test.log) && continue
  (jing bdk.rng $x &>>test.log) && continue
  echo ok
  ok=1
  pass=$((pass+1))
done
test $ok -eq 0 && echo fail
echo $pass of $pass_tests tests OK

# Don't save the log on success.
if [ $pass -eq $pass_tests ]; then
  rm test.log
else
  echo "More details in $PWD/test.log"
fi
