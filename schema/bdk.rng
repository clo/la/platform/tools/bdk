<?xml version="1.0" encoding="UTF-8"?>
<!--
      Copyright (C) 2016 The Android Open Source Project

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.

      Project specification schema for the Brillo Developer Kit.
  -->
<grammar xmlns="http://relaxng.org/ns/structure/1.0"
         datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes">

  <!-- Expect a project root element, but allow packs and targets to
       be added as choices upon inclusion.
    -->
  <start combine="choice">
    <ref name="Project"/>
  </start>

  <!-- Pull in start nodes and definitions for Packs
       and Targets.
       The current recommendation is to only include a
       new file if it is:
       (a) Common across all inclusions
       (b) Represents a possible standalone document schema.
    -->
  <include href="packs.rng"/>
  <include href="targets.rng"/>

  <define name="DirsPath">
    <data type="token">
      <param name="maxLength">4096</param>
    </data>
  </define>

  <!-- Dirs -->
  <define name="Config">
    <element name="config">
      <group>
        <optional>
          <element name="cache-dir">
            <attribute name="path">
              <ref name="DirsPath"/>
            </attribute>
          </element>
        </optional>
        <optional>
          <element name="output-dir">
            <attribute name="path">
              <ref name="DirsPath"/>
            </attribute>
          </element>
        </optional>
        <optional>
          <element name="default">
            <attribute name="target">
              <ref name="TargetName"/>
            </attribute>
          </element>
        </optional>
      </group>
    </element>
  </define>

  <define name="Project">
    <element name="project">
      <group>
        <!-- Optionally indicate which version of the schema is in use -->
        <optional>
          <attribute name="version">
            <choice>
              <value type="integer">1</value>
            </choice>
          </attribute>
        </optional>
        <!-- At most, one <config> may be defined. -->
        <optional>
          <ref name="Config"/>
        </optional>
        <!-- Require at least one packs and one targets. -->
        <interleave>
          <oneOrMore>
            <ref name="Targets"/>
          </oneOrMore>
          <oneOrMore>
            <ref name="Packs"/>
          </oneOrMore>
        </interleave>
        <zeroOrMore>
          <choice>
            <ref name="Targets"/>
            <ref name="Packs"/>
          </choice>
        </zeroOrMore>
      </group>
    </element>
  </define>
</grammar>
