#
# Copyright 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
# * Overview
# This file enables an Android product tree to be lifted wholesale
# out of the Android source tree and built using the following command:
#
#  make -f path/to/bdk/tools/bdk/build/wrap-product.mk
#
# A BUILDTYPE= value may be added to select between eng, user, and userdebug
# builds. 'eng' is the default.
#
# * Additional usage:
#
# Calling with ENVCMD set, will invoke that command from the current
# working directory using the command from Android's envsetup. This
# is often used to invoke incremental builds, like:
#
#   make -f $(BDK_PATH)/tools/bdk/build/wrap-product.mk ENVCMD=mm -j8
#
#
# * Product definitions:
#
# AndroidProducts.mk and <product>.mk are required just as they are
# for any product.  vendorsetup.sh is not necessary
# but may be present (it will be ignored).
#
#
# * Additional information:
#
# See create-shadow-bdk for more details on what this file does.
#
# The following variables may be overriden by the caller but it
# is not recommended:
# - BDK_PATH: the path to the BDK install
# - PRODUCT_DIR: the path containing a product definition
# - PRODUCT_NAME: the configured name of the product
# - PRODUCT_DEVICE: the configured device of the product
# - PRODUCT_MANUFACTURER: the configured brand of the product
#

# Discover the BDK if it isn't defined.
BDK_PATH ?= $(dir $(abspath $(dir $(abspath $(dir $(abspath \
               $(dir $(abspath \
                  $(filter %/wrap-product.mk,$(MAKEFILE_LIST))))))))))
ifeq ($(BDK_PATH),)
  $(error BDK_PATH is not defined.)
endif

include $(BDK_PATH)/tools/bdk/build/wrap-common.mk

# If no product directory was passed in, walk upward looking for AndroidProducts.mk.
PRODUCT_DIR ?= $(shell sh -c 'while test "$$PWD" != "/"; do \
  (test -e AndroidProducts.mk && echo $$PWD && exit 0); \
  cd ..; done')

ifeq ($(PRODUCT_DIR),)
  $(error PRODUCT_DIR not defined or discoverable.)
endif

CFGTREE_ROOT := $(PRODUCT_DIR)
include $(BDK_PATH)/device/generic/brillo/cfgtree.mk

PRODUCT_NAME ?= $(call cfgtree_get,name,optional)
# If the name is not supplied, it is extracted from the name of the <product>.mk
# included in AndroidProducts.mk. If we later want to hide this file, we will still
# need a configurable replacement.
ifeq ($(PRODUCT_NAME),)
  LOCAL_DIR := $(PRODUCT_DIR)
  include $(PRODUCT_DIR)/AndroidProducts.mk
  # Always grab the first .mk entry from the list.
  PRODUCT_NAME := $(basename $(notdir $(firstword $(filter %.mk,$(PRODUCT_MAKEFILES)))))
  PRODUCT_MAKEFILES :=
  LOCAL_DIR :=
endif

ifeq ($(PRODUCT_NAME),)
  $(error PRODUCT_NAME not supplied or discoverable.)
endif

# Extract the PRODUCT_DEVICE from <product>.mk if possible.
ifeq ($(PRODUCT_DEVICE),)
  $(eval $(shell grep '^PRODUCT_DEVICE :=' "$(PRODUCT_DIR)/$(PRODUCT_NAME).mk"))
endif
# Catch failure to supply or extract the device.
ifeq ($(PRODUCT_DEVICE),)
  $(error PRODUCT_DEVICE not supplied.)
endif

# Default the out path to live under the product directory.
PRODUCT_OUT ?= $(PRODUCT_DIR)/out
$(info Output will be placed in $(PRODUCT_OUT))

ifeq ($(PRODUCT_MANUFACTURER),)
  $(eval $(shell grep '^PRODUCT_MANUFACTURER :=' "$(PRODUCT_DIR)/$(PRODUCT_NAME).mk"))
endif
# Just default to 'brillo' if not supplied.
PRODUCT_MANUFACTURER ?= brillo

# By default, all builds are eng builds.
BUILDTYPE ?= eng

# By default, Android build expects Java.
BRILLO_NO_JAVA ?=

HERE ?= $(PWD)

# Create the shadow BDK in the product out directory. Use a dot path to
# discourage accidental damage.
PRODUCT_BDK := $(PRODUCT_OUT)/.bdk

PHONY: $(MAKECMDGOALS)

_all::
%::
	@$(R) rm -rf "$(PRODUCT_BDK)" # Cheap to rebuild
	$(call create-shadow-bdk,$(BDK_PATH),$(PRODUCT_BDK))
ifeq ($(BRILLO_NO_JAVA),1)
	$(call fake-java,$(PRODUCT_BDK))
endif
	$(call inject-product,$(PRODUCT_DIR),$(PRODUCT_BDK),$(PRODUCT_OUT),$(PRODUCT_MANUFACTURER),$(PRODUCT_NAME))
ifneq ($(ENVCMD),)
	+$(call build-product-here,$(BDK_PATH),$(PRODUCT_BDK),$(PRODUCT_NAME),$(PRODUCT_DEVICE),$(PRODUCT_OUT),$(BUILDTYPE),$(HERE),$(ENVCMD))
else
	+$(call build-product,$(BDK_PATH),$(PRODUCT_BDK),$(PRODUCT_NAME),$(PRODUCT_DEVICE),$(PRODUCT_OUT),$(BUILDTYPE))
endif
